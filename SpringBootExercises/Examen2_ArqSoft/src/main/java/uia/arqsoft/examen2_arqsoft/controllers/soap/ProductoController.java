package uia.arqsoft.examen2_arqsoft.controllers.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uia.arqsoft.examen2_arqsoft.models.Producto;
import uia.arqsoft.examen2_arqsoft.services.ProductoService;

@Controller
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar")
    public String listarProductos(Model modelo){
        modelo.addAttribute("listaProductos", productoService.getProductos());
        return "productos_lista";
    }

    @GetMapping("/agregar")
    public String agregarProducto(Model modelo){
        modelo.addAttribute("producto", new Producto());
        modelo.addAttribute("listaCategorias", productoService.getCategorias());
        return "producto_formulario";
    }

    @PostMapping("/guardar")
    public String guardarProducto(Producto producto){
        productoService.guardarProducto(producto);
        return "redirect:/productos/listar";
    }

    @GetMapping("/editar/{id}")
    public String editarProducto(@PathVariable("id") Integer id, Model modelo){
        Producto producto = productoService.getProductoById(id);
        modelo.addAttribute("producto", producto);
        modelo.addAttribute("listaCategorias", productoService.getCategorias());
        return "producto_formulario";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarProducto(@PathVariable("id") Integer id){
        productoService.eliminarProducto(id);
        return "redirect:/productos/listar";
    }


}

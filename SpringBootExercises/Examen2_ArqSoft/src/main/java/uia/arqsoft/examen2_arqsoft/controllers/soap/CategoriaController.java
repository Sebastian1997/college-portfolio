package uia.arqsoft.examen2_arqsoft.controllers.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uia.arqsoft.examen2_arqsoft.models.Categoria;
import uia.arqsoft.examen2_arqsoft.services.CategoriaService;

import java.util.List;

@Controller
@RequestMapping("/categorias")
public class CategoriaController {
    @Autowired
    private CategoriaService categoriaService;

    @GetMapping("/mostrar")
    public String mostrarCategorias(Model model){
        List<Categoria> listaCategorias = categoriaService.getCategorias();
        model.addAttribute("listaCategorias", listaCategorias);
        return "categorias_lista";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarCategoria(@PathVariable("id") Integer id){
        categoriaService.eliminarCategoria(id);
        return "redirect:/categorias/mostrar";
    }

    @GetMapping("/nuevo")
    public String mostrarFormularioNuevaCategoria(Model model){
        model.addAttribute("categoria", new Categoria());
        return "categoria_form";
    }

    @PostMapping("/guardar")
    public String guardarCategoria(Categoria categoria){
        categoriaService.guardarCategoria(categoria);
        return "redirect:/categorias/mostrar";
    }

    @GetMapping("/editar/{id}")
    public String mostrarFormularioEditarCategoria(@PathVariable("id") Integer id, Model model){
        Categoria categoria = categoriaService.getCategoriaPorId(id);
        model.addAttribute("categoria", categoria);
        return "categoria_form";
    }


}

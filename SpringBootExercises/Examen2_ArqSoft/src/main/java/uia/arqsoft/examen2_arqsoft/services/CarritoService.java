package uia.arqsoft.examen2_arqsoft.services;

import jakarta.persistence.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.examen2_arqsoft.models.Carrito;
import uia.arqsoft.examen2_arqsoft.models.Producto;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.repositories.CarritoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CarritoService {
    @Autowired
    private CarritoRepository repository;

    @Autowired
    private ProductoService productoService;

    public List<Producto> getAllProductos(){
        return productoService.getProductos();
    }

    public List<Carrito> getCarritosByUserId(Integer id){
        return repository.findByUsuarioId(id);
    }

    public Carrito createCarrito(Carrito carrito){
        return repository.save(carrito);
    }

    public void deleteCarrito(Carrito carrito){
        repository.delete(carrito);
    }

    public List<Carrito> getCarritoByUsuario(Integer id){

        return repository.findByUsuarioId(id);
    }


}

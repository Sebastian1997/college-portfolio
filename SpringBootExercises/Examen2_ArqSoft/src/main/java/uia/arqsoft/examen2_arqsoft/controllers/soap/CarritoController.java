package uia.arqsoft.examen2_arqsoft.controllers.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uia.arqsoft.examen2_arqsoft.models.Carrito;
import uia.arqsoft.examen2_arqsoft.models.Producto;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.services.CarritoService;
import uia.arqsoft.examen2_arqsoft.services.ProductoService;
import uia.arqsoft.examen2_arqsoft.services.UsuarioService;

import java.util.List;

@Controller
@RequestMapping("/carrito")
public class CarritoController {

    @Autowired
    CarritoService carritoService;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    ProductoService productoService;

    @GetMapping("/catalogo/{userid}")
    public String mostrarCatalogo(@PathVariable (name = "userid") Integer id, Model modelo){
        List<Producto> listaProductos = productoService.getProductos();
        Usuario user = usuarioService.getUsuarioById(id);
        modelo.addAttribute("listaProductos", listaProductos);
        modelo.addAttribute("usuario", user);
        modelo.addAttribute("carrito", new Carrito(user, null));
        return "catalogo";
    }


    @PostMapping("/anadir/{id}")
    public String anadirProducto(@PathVariable Integer id, Carrito carrito){
        carrito.setUsuario(usuarioService.getUsuarioById(id));
        carritoService.createCarrito(carrito);
        return "redirect:/carrito/catalogo/" + id;
    }

    @GetMapping("/ver/{id}")
    public String verCarrito(@PathVariable Integer id, Model modelo){
        List<Carrito> listaCarrito = carritoService.getCarritoByUsuario(id);
        modelo.addAttribute("listaCarrito", listaCarrito);
        modelo.addAttribute("usuario", usuarioService.getUsuarioById(id));
        return "carrito";
    }





}

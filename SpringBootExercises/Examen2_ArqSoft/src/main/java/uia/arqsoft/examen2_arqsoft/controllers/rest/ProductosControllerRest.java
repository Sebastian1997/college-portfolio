package uia.arqsoft.examen2_arqsoft.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.examen2_arqsoft.models.Producto;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.services.ProductoService;

import java.util.List;

@RestController
@RequestMapping("/prod")
public class ProductosControllerRest {
    @Autowired
    private ProductoService productoService;

    @GetMapping()
    public ResponseEntity<List<Producto>> getAllProductos(){
        return new ResponseEntity(productoService.getProductos(), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<Producto> createProducto(@RequestBody Producto producto){
        return  new ResponseEntity<>(productoService.guardarProducto(producto), HttpStatus.CREATED);
    }

    @PutMapping("/{productoId}")
    public ResponseEntity<Producto> updateProducto(@PathVariable("productoId") Integer productoId, @RequestBody Producto producto){
        return new ResponseEntity<>(productoService.updateProducto(productoId, producto), HttpStatus.CREATED);
    }

    @DeleteMapping("/{productoId}")
    public ResponseEntity<Void> deleteProducto(@PathVariable("productoId") Integer productoId){
        productoService.eliminarProducto(productoId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

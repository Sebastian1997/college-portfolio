package uia.arqsoft.examen2_arqsoft.controllers.soap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainAppController {

        @RequestMapping("/")
        public String index() {
            return "index";
        }

}

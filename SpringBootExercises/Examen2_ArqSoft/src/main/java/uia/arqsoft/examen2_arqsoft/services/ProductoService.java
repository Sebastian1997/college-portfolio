package uia.arqsoft.examen2_arqsoft.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.examen2_arqsoft.models.Categoria;
import uia.arqsoft.examen2_arqsoft.models.Producto;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.repositories.ProductoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private CategoriaService categoriaService;

    public List<Producto> getProductos(){
        return productoRepository.findAll();
    }

    public List<Categoria> getCategorias(){
        return categoriaService.getCategorias();
    }

    public Producto guardarProducto(Producto producto){
        return productoRepository.save(producto);
    }


    public Producto getProductoById(Integer id){
        return productoRepository.findById(id).get();
    }

    public void eliminarProducto(Integer id){
        Optional<Producto> result = productoRepository.findById(id);
        if(result.isPresent()){
            productoRepository.deleteById(id);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Producto con id %d no encontrado", id));
        }

    }


    public Producto updateProducto(Integer id, Producto producto){
        Producto productoActual = productoRepository.findById(id).get();
        productoActual.setNombre(producto.getNombre());
        productoActual.setPrecio(producto.getPrecio());
        productoActual.setCategoria(producto.getCategoria());
        return productoRepository.save(productoActual);
    }
}

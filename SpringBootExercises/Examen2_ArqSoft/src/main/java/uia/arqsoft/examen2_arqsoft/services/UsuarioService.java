package uia.arqsoft.examen2_arqsoft.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.repositories.UsuarioRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public List<Usuario>getAllUsuarios(){
        return repository.findAll();
    }

    public Usuario getUsuarioById(Integer id){
        return repository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id)));
    }

    public Usuario createUsuario(Usuario usuario){
        return repository.save(usuario);
    }

    public Usuario updateUsuario(Integer id, Usuario usuario){
        Optional<Usuario> result = repository.findById(id);
        if(result.isPresent()){
            return repository.save(usuario);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id));
        }
    }


    public void deleteUsuario(Integer id){
        Optional<Usuario> result = repository.findById(id);
        if(result.isPresent()){
            repository.deleteById(id);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id));
        }
    }





}

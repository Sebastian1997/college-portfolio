package uia.arqsoft.examen2_arqsoft.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.examen2_arqsoft.models.Categoria;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.services.CategoriaService;

import java.util.List;

@RestController
@RequestMapping("/cat")
public class CategoriaRestController {
    @Autowired
    private CategoriaService categoriaService;


    @GetMapping()
    public ResponseEntity<List<Categoria>> getCategorias(){
        return new ResponseEntity(categoriaService.getAllCategorias(), HttpStatus.OK);
    }

    @PostMapping()
    public void createCategoria(@RequestBody Categoria categoria){
        categoriaService.guardarCategoria(categoria);
    }

    @PutMapping("/{categoriaId}")
    public ResponseEntity<Categoria> updateCategoria(@PathVariable("categoriaId") Integer categoriaId, @RequestBody Categoria categoria){
        return new ResponseEntity<Categoria>(categoriaService.updateCategoria(categoriaId, categoria), HttpStatus.CREATED);
    }

    @DeleteMapping("/{usuarioId}")
    public ResponseEntity<Void> deleteUsuario(@PathVariable("usuarioId") Integer usuarioId){
        categoriaService.eliminarCategoria(usuarioId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }



}

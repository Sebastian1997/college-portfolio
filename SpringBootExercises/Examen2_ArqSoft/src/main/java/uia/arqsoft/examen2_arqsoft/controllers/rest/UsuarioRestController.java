package uia.arqsoft.examen2_arqsoft.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.services.UsuarioService;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioRestController {
    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("")
    public ResponseEntity<List<Usuario>> getUsuarios(){
        return new ResponseEntity(usuarioService.getAllUsuarios(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Usuario> createUsuario(@RequestBody Usuario usuario){
        return new ResponseEntity<Usuario>(usuarioService.createUsuario(usuario), HttpStatus.CREATED);
    }

    @PutMapping("/{usuarioId}")
    public ResponseEntity<Usuario> updateUsuario(@PathVariable("usuarioId") Integer usuarioId, @RequestBody Usuario usuario){
        return new ResponseEntity<Usuario>(usuarioService.updateUsuario(usuarioId, usuario), HttpStatus.CREATED);
    }

    @DeleteMapping("/{usuarioId}")
    public ResponseEntity<Void> deleteUsuario(@PathVariable("usuarioId") Integer usuarioId){
        usuarioService.deleteUsuario(usuarioId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }







}

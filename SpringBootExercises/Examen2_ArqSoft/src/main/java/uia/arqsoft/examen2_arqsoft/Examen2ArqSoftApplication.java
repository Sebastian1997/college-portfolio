package uia.arqsoft.examen2_arqsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Examen2ArqSoftApplication {

    public static void main(String[] args) {
        SpringApplication.run(Examen2ArqSoftApplication.class, args);
    }

}

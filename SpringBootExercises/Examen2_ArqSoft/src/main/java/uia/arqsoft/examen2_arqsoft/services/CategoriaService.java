package uia.arqsoft.examen2_arqsoft.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.examen2_arqsoft.models.Categoria;
import uia.arqsoft.examen2_arqsoft.repositories.CategoriaRepository;
import java.util.List;
import java.util.Optional;

@Service
public class CategoriaService {
    @Autowired
    private CategoriaRepository repository;

    public ResponseEntity<List<Categoria>> getAllCategorias(){
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);

    }

    public List<Categoria> getCategorias (){
        return repository.findAll();
    }

    public void eliminarCategoria(Integer id){
        Optional<Categoria> result = repository.findById(id);
        if(result.isPresent()){
            repository.deleteById(id);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Categoria con id %d no encontrado", id));
        }
    }


    public void guardarCategoria(Categoria categoria){

        if(categoria.getId()==null) {
            repository.save(categoria);
        }
        else{
            Categoria categoriaExistente = repository.findById(categoria.getId()).get();
            categoriaExistente.setNombre(categoria.getNombre());
            repository.save(categoriaExistente);
        }
    }

    public Categoria getCategoriaPorId(Integer id){
        return repository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Categoria con id %d no encontrado", id)));
    }


    public Categoria updateCategoria(Integer id, Categoria categoria)
    {
        Optional<Categoria> result = repository.findById(id);
        if(result.isPresent()){
            Categoria categoriaExistente = repository.findById(id).get();
            categoriaExistente.setNombre(categoria.getNombre());
            return repository.save(categoriaExistente);
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Categoria con id %d no encontrado", id));
        }

    }

}

package uia.arqsoft.examen2_arqsoft.controllers.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uia.arqsoft.examen2_arqsoft.models.Usuario;
import uia.arqsoft.examen2_arqsoft.services.UsuarioService;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

        @Autowired
        private UsuarioService usuarioService;

        @GetMapping("/listar")
        public String listarUsuarios(Model modelo){
            modelo.addAttribute("listaUsuarios", usuarioService.getAllUsuarios());
            return "usuarios_lista";
        }

        @GetMapping("/agregar")
        public String agregarUsuario(Model modelo){
            modelo.addAttribute("usuario", new Usuario());
            return "usuario_form";
        }

        @PostMapping("/guardar")
        public String guardarUsuario(Usuario usuario){
            if (usuario.getId() != null){
                usuarioService.updateUsuario(usuario.getId(), usuario);
            }else{
            usuarioService.createUsuario(usuario);
            }
            return "redirect:/usuarios/listar";
        }

        @GetMapping("/editar/{id}")
        public String editarUsuario(@PathVariable("id") Integer id, Model modelo){
            modelo.addAttribute("usuario", usuarioService.getUsuarioById(id));
            return "usuario_form";
        }





}

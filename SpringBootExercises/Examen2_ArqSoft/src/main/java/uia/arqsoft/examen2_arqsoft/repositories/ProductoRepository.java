package uia.arqsoft.examen2_arqsoft.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.examen2_arqsoft.models.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer>{
}

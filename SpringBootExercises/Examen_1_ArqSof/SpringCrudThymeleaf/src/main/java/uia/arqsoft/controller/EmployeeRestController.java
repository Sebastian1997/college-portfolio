package uia.arqsoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.model.Employee;
import uia.arqsoft.service.EmployeeService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class EmployeeRestController {
        @Autowired
        private EmployeeService employeeService;

        // get all employees
        @GetMapping("/employees")
        public List<Employee> getAllEmployees(){
            return employeeService.listAllEmployees();
        }
        @PostMapping("/employees")
        public Employee createEmployee(@RequestBody Employee employee){
        return employeeService.saveEmployee(employee);
        }

        //get employee by id rest api
        @GetMapping("/employees/{id}")
        public ResponseEntity<Employee> getEmployeeById(@PathVariable() Long id) {
                Employee employee = employeeService.getEmployeeById(id);
                return ResponseEntity.ok(employee);
        }

        // update employee rest api
        @PutMapping("/employees/{id}")
        public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @RequestBody Employee employeeDetails){
                Employee employee = employeeService.getEmployeeById(id);
                employee.setFirstName(employeeDetails.getFirstName());
                employee.setLastName(employeeDetails.getLastName());
                employeeDetails.setEmailId(employeeDetails.getEmailId());

                Employee updatedEmployee = employeeService.saveEmployee(employee);
                return ResponseEntity.ok(updatedEmployee);
        }


}

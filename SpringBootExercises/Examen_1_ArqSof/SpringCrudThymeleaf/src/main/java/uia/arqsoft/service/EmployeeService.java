package uia.arqsoft.service;

import uia.arqsoft.model.Employee;

import java.util.List;

public interface EmployeeService {

    public List<Employee> listAllEmployees();
    public Employee saveEmployee(Employee employee);

    public Employee getEmployeeById(Long id);
    public Employee updateEmployee(Employee employee);
    public void deleteEmployee(Long id);
}

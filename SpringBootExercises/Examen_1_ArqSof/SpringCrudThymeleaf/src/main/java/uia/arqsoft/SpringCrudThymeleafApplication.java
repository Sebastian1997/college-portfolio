package uia.arqsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import uia.arqsoft.model.Employee;
import uia.arqsoft.repository.EmployeeRepository;

@SpringBootApplication
public class SpringCrudThymeleafApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringCrudThymeleafApplication.class, args);
    }

    @Autowired
    private EmployeeRepository repository;
    @Override
    public void run(String... args) throws Exception {

    }
}

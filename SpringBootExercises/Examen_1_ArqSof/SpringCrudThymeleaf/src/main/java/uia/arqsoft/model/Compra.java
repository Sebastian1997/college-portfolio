package uia.arqsoft.model;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "compra")
public class Compra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "total")
    private Float total;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private Employee employee;

    public Compra() {
    }

    public Compra(Long id, Date date, Float total, String descripcion, Employee employee) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.description = descripcion;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getDescripcion() {
        return description;
    }

    public void setDescripcion(String descripcion) {
        this.description = descripcion;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}


package uia.arqsoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}

import React, { Component } from 'react';
import EmployeeService from '../services/EmployeeService';
import {Link, useNavigate} from 'react-router-dom';



class ListEmployeeComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            employees: []
        }

    }

    componentDidMount(){
        EmployeeService.getEmployees().then((res) => {
            this.setState({ employees: res.data});
        });
    }



    render() {
        return (
            <div>
                <h2 className="text-center">Employees List</h2>
                <div className='row'>
                    <Link to = "/add-employee" className='btn btn-primary'>Add</Link>

                </div>
                <div className="row">
                    <table className="table table-stripted table-bordered">
                        <thead>
                            <tr>
                                <th> Employee First Name</th>
                                <th> Employee Last Name</th>
                                <th> Employee EmailId</th>
                                <th> Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.employees.map(
                                    employee =>
                                    <tr key = {employee.id}>
                                            <td> {employee.firstName} </td>
                                            <td> {employee.lastName} </td>
                                            <td> {employee.emailId} </td>
                                            <td>
                                                <Link 
                                                    to={'/update-employee'}
                                                    state={{ from: ""}} className='btn btn-info'>
                                                    Update
                                                </Link>
                                            </td>
                                        
                                    </tr>
                                )
                            }


                        </tbody>

                    </table>
                </div>
                
            </div>
        );
    }
}

export default ListEmployeeComponent;
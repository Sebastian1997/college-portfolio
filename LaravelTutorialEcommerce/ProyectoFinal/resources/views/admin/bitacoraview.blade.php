<div class="main-panel">
    <div class="content-wrapper">




      <div class="row ">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Status de la orden <a style="margin-left: 30px;" class="btn btn-primary" href="/bitacora/export">Exportar excel</a></h4>
              
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>
                        <div class="form-check form-check-muted m-0">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input">
                          </label>
                        </div>
                      </th>
                      <th> Nombre del cliente</th>
                      <th> Folio Orden </th>
                      <th> Total </th>
                      <th> Pago </th>
                      <th> Fecha de pedido </th>
                      <th> Estatus del pedido </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orders as $order)
                    <tr>
                      <td>
                        <div class="">
                            <a href="{{ url('/bitacora/delete', $order->id) }}"><button class="btn btn-primary">Hide</button></a>

                        </div>
                      </td>
                      <td>
                        <span class="pl-2">{{$order->user_name}}</span>
                      </td>
                      <td> {{$order->invoice}} </td>
                      <td> {{$order->total}} </td>
                      <td> {{$order->payment_method}} </td>
                      <td> {{$order->created_at}} </td>
                      
                      <td>

                        <div class="badge badge-outline-warning">{{$order->delivery_status}}</div>

                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      



    







      
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin templates</a> from Bootstrapdash.com</span>
      </div>
    </footer>
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>

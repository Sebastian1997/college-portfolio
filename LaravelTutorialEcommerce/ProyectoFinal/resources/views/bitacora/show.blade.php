@extends('layouts.app')

@section('template_title')
    {{ $bitacora->name ?? "{{ __('Show') Bitacora" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Bitacora</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('bitacoras.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>User Name:</strong>
                            {{ $bitacora->user_name }}
                        </div>
                        <div class="form-group">
                            <strong>Invoice:</strong>
                            {{ $bitacora->invoice }}
                        </div>
                        <div class="form-group">
                            <strong>Total:</strong>
                            {{ $bitacora->total }}
                        </div>
                        <div class="form-group">
                            <strong>Payment Method:</strong>
                            {{ $bitacora->payment_method }}
                        </div>
                        <div class="form-group">
                            <strong>Delivery Status:</strong>
                            {{ $bitacora->delivery_status }}
                        </div>
                        <div class="form-group">
                            <strong>Saved:</strong>
                            {{ $bitacora->saved }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Bitacora;
use App\Models\Orderproduct;
use Session;
use Stripe;
use RealRashid\SweetAlert\Facades\Alert;



class HomeController extends Controller
{



    public function index()
    {
        $product=Product::paginate(3);
        return view('home.userpage', compact('product'));
    }
    public function redirect()
    {
        $usertype=Auth::user()->usertype;

        if($usertype=='1'){
            $orders = Order::all();
            $users = User::all();
            $totalStripe = 0;
            $totalCash = 0;
            $mexico = 0;
            $usa = 0;
            $canada = 0;
            $otros = 0;
            $totalusers = User::all()->count();
            $lowstock = Product::where('quantity', '<', 10)->get();
            $totalproducts = Product::all()->count();
            $currentuser = Auth::user()->name;
            
            foreach($orders as $order){
                if($order->payment_status == 'Paid'){
                    $totalStripe = $totalStripe + $order->total;
                }
                else{
                    $totalCash = $totalCash + $order->total;
                }
            }

            foreach($users as $user){
                if($user->country == 'Mexico'){
                    $mexico = $mexico + 1;
                }
                elseif($user->country == 'USA'){
                    $usa = $usa + 1;
                }
                elseif($user->country == 'Canada'){
                    $canada = $canada + 1;
                }
                else{
                    $otros = $otros + 1;
                }
            }

            $counts = [$totalStripe, $totalCash];

            $datos = [$mexico, $usa, $canada, $otros];
            $orders = Order::all();
            return view('admin.home', compact('currentuser','datos', 'counts', 'orders', 'totalproducts', 'lowstock', 'totalusers'));
        }
        else{
            $product=Product::paginate(3);
            return view('home.userpage', compact('product'));
            
        }
    }

    public function product_details($id){
        $product = Product::find($id);
        return view('home.product_details', compact('product'));
    }

    public function add_cart(Request $request, $id){
       
        if(Auth::id()){

            $user = User::find(Auth::id());
            $product = Product::find($id);
            if($product->discount_price == NULL){
                $subtotal = $product->price * $request->quantity;
            }
            else{
                $subtotal = $product->discount_price * $request->quantity;
            }
            $cart = new Cart();
            $cart->user_id = $user->id;
            $cart->product_id = $product->id;
            $cart->quantity = $request->quantity;
            $cart->subtotal = $subtotal;
            $cart->save();

            Alert::info('Producto agregado correctamente', 'Hemos agregado el producto al carrito');
            return redirect()->back();
        }
        else{
            return redirect('login');
        }
    }

    public function show_cart(){
        if(Auth::id()){
        $user = User::find(Auth::id());
        $carts = Cart::where('user_id', $user->id)->get();
        return view('home.showcart', compact('carts'));
        }
        else{
            return redirect('login');
        }
    }

    public function remove_cart($id){
        $cart = Cart::find($id);
        $cart->delete();
        return redirect()->back()->with('message', 'Producto eliminado del carrito');
    }

    public function cash_order(){
        $user=Auth::user();
        // Generar una cadena con letras y números
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // Mezclar la cadena de caracteres
        $mixed = str_shuffle($characters);
        // Tomar los primeros 15 caracteres
        $folio = substr($mixed, 0, 15);

        $userid=$user->id;
        $order = new Order();
        $order->user_id = $userid;
        $order->payment_status = 'cash';
        $order->delivery_status = 'processing';
        $order->invoice = $folio;
        $order->save();
        $total = 0;


        $carts = Cart::where('user_id', $userid)->get();
        foreach($carts as $cart){
            $orderproduct = new Orderproduct();
            $product = Product::find($cart->product_id);
            $product->quantity = $product->quantity - $cart->quantity;
            $product->save();
            $orderproduct->order_id = $order->id;
            $orderproduct->product_id = $cart->product_id;
            $orderproduct->quantity = $cart->quantity;
            $orderproduct->subtotal = $cart->subtotal;
            $orderproduct->save();
            $total = $total + $cart->subtotal;
        }
        $order->total = $total;
        $order->save();

        //Guarda en bitacora
        $bitacora = new Bitacora();
        $bitacora->user_name = $order->user->name;
        $bitacora->invoice = $order->invoice;
        $bitacora->total = $order->total;
        $bitacora->payment_method = $order->payment_status;
        $bitacora->delivery_status = $order->delivery_status;
        $bitacora->save();

        Cart::where('user_id', $userid)->delete();
        return redirect()->back()->with('message', 'Hemos recibido tu pedido, pronto nos pondremos en contacto contigo');
        
    }


    //Codigo de stripe


    public function stripe($totalprice){

        return view('home.stripe', compact('totalprice'));
    }





    public function stripePost(Request $request, $totalprice)
    {

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    
        Stripe\Charge::create ([
                "amount" => $totalprice * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Thanks for payment" 
        ]);


        $user=Auth::user();
        // Generar una cadena con letras y números
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // Mezclar la cadena de caracteres
        $mixed = str_shuffle($characters);
        // Tomar los primeros 15 caracteres
        $folio = substr($mixed, 0, 15);

        

        $userid=$user->id;
        $order = new Order();
        $order->user_id = $userid;
        $order->payment_status = 'Paid';
        $order->delivery_status = 'processing';
        $order->invoice = $folio;
        $order->save();
        $total = 0;


        $carts = Cart::where('user_id', $userid)->get();
        foreach($carts as $cart){
            $orderproduct = new Orderproduct();
            $product = Product::find($cart->product_id);
            $product->quantity = $product->quantity - $cart->quantity;
            $product->save();
            $orderproduct->order_id = $order->id;
            $orderproduct->product_id = $cart->product_id;
            $orderproduct->quantity = $cart->quantity;
            $orderproduct->subtotal = $cart->subtotal;
            $orderproduct->save();
            $total = $total + $cart->subtotal;
        }
        $order->total = $total;
        $order->save();
        

               //Guarda en bitacora
               $bitacora = new Bitacora();
               $bitacora->user_name = $order->user->name;
               $bitacora->invoice = $order->invoice;
               $bitacora->total = $order->total;
               $bitacora->payment_method = $order->payment_status;
               $bitacora->delivery_status = $order->delivery_status;
               $bitacora->save();

        Cart::where('user_id', $userid)->delete();
      
        Session::flash('success', 'Pago exitoso, pronto nos pondremos en contacto!');
              
        return back();
    }


}

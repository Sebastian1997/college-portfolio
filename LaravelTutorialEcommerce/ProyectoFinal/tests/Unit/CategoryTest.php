<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use App\Models\Category;
use App\Http\Controllers\AdminController;


class CategoryTest extends TestCase
{
    use DatabaseTransactions;


    public function test_add_category_endpoint()
    {
        $categoryData = [
            'category' => 'Nueva categoría'
        ];

        $response = $this->post('/add_category', $categoryData);

        $response->assertStatus(302);

        $this->assertDatabaseHas('categories', [
            'category_name' => 'Nueva categoría'
        ]);
    }


}

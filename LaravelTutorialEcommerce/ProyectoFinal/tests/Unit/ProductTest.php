<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Models\Category;
use App\Models\Product;

class ProductTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /** @test */
    public function add_product_successfully()
    {
        Storage::fake('public');

        $category = Category::factory()->create();

        $data = [
            'title' => $this->faker->word(),
            'description' => $this->faker->sentence(),
            'price' => $this->faker->randomFloat(2, 1, 1000),
            'dis_price' => $this->faker->randomFloat(2, 1, 1000),
            'quantity' => $this->faker->numberBetween(1, 1000),
            'category' => $category->id,
            'image' => UploadedFile::fake()->image('product.jpg'),
        ];

        $response = $this->post('/add_product', $data);

        $response->assertRedirect();
        $response->assertSessionHas('message', 'Producto añadido con exito');

        $this->assertDatabaseHas('products', [
            'title' => $data['title'],
            'description' => $data['description'],
            'price' => $data['price'],
            'discount_price' => $data['dis_price'],
            'quantity' => $data['quantity'],
            'category_id' => $data['category'],

        ]);

    }

    /** @test */

    public function update_product_successfully()
    {
        Storage::fake('public');

        $category = Category::factory()->create();

        $data = [
            'title' => $this->faker->word(),
            'description' => $this->faker->sentence(),
            'price' => $this->faker->randomFloat(2, 1, 1000),
            'dis_price' => $this->faker->randomFloat(2, 1, 1000),
            'quantity' => $this->faker->numberBetween(1, 1000),
            'category' => $category->id,
            'image' => UploadedFile::fake()->image('product.jpg'),
        ];

        $response = $this->post('/add_product', $data);

        $product = Product::first();

        $dataupload = [
            'title' => $this->faker->word(),
            'description' => $this->faker->sentence(),
            'price' => $this->faker->randomFloat(2, 1, 1000),
            'dis_price' => $this->faker->randomFloat(2, 1, 1000),
            'quantity' => $this->faker->numberBetween(1, 1000),
            'category' => $category->id,
            'image' => null,
        ];


        $response = $this->post('/update_product_confirm/' . $product->id, $dataupload);

        // Refresh the product from the database
        $product->refresh();

        // Assert that the product attributes were updated correctly
        $this->assertEquals($dataupload['title'], $product->title);
        $this->assertEquals($dataupload['description'], $product->description);
        $this->assertEquals($dataupload['price'], $product->price);
        $this->assertEquals($dataupload['dis_price'], $product->discount_price);
        $this->assertEquals($dataupload['category'], $product->category_id);
        $this->assertEquals($dataupload['quantity'], $product->quantity);
        $this->assertNotNull($product->image);

    }


    
}

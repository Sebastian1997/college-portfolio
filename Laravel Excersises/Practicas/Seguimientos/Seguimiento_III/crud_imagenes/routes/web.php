<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;


Route::get('/', [ProductosController::class, 'index'])->name('productos.index');
Route::get('/create', [ProductosController::class, 'create'])->name('productos.create');
Route::post('/store', [ProductosController::class, 'store'])->name('productos.store');
Route::get('/edit/{id}', [ProductosController::class, 'edit'])->name('productos.editar');
Route::put('/update/{id}', [ProductosController::class, 'update'])->name('productos.update');
Route::get('/show/{id}', [ProductosController::class, 'show'])->name('productos.show');
Route::delete('/destroy/{id}', [ProductosController::class, 'destroy'])->name('productos.destroy');
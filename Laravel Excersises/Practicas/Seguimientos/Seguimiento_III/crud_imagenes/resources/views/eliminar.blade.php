@extends('layout/plantilla')

@section('tituloPagina', 'Crear un nuevo registro')

@section('contenido')

    <br>

    <div class="container">
    <div class="card">
    <div class="card-header">
    </div>
    <div class="card-body">
      <h5 class="card-title">Eliminar producto</h5>
      <p class="card-text">
        
        <div class="alert" role="alert">
            <table class="table table-sm table-hover">
                <thead>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $producto->nombre }}</td>
                        <td>{{ $producto->descripcion }}</td>
                        <td>{{ $producto->precio }}</td>
                        <td>{{ $producto->cantidad }}</td>
                    </tr>
                </tbody>
            </table>
            <hr>

            <form action="{{ route('productos.destroy', $producto->id) }}" method="POST">
              @csrf
              @method('DELETE')
                <a href="{{ route('productos.index') }}" class=" btn btn-info">Regresar</a>
                <button class="btn btn-danger">Eliminar</button>
            </form>

        </div>

        
      </p>
      
    </div>
  </div>
  </div>

@endsection
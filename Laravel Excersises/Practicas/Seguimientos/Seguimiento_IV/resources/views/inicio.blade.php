@extends('layout/plantilla')

@section('tituloPagina', 'Crud con Laravel')

@section('contenido')

    <br>
    <br>
    <div class="card text-bg-dark">
    <div class="card-header h1 ">
        Productos
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col-sm-12">
                @if ($mensaje = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        Producto agregado con exito
                  </div>   
                @endif

            </div>
        </div>

        <hr>
        <p class="card-text">
            <div class="table table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </thead>
                    <tbody>
                        @foreach ($datos as $item)
                        <tr>
                            <td>{{ $item->nombre}}</td>
                            <td>{{ $item->descripcion}}</td>
                            <td>{{ $item->precio}}</td>
                            <td>{{ $item->cantidad}}</td>
                            <td>
                                
                                <form action="{{ route("productos.editar", $item->id)}}" method="GET">
                                    <button class="btn btn-outline-warning">
                                        <span class="fas fa-edit"></span>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route("productos.show", $item->id)}}" method="GET">
                                    <button class="btn btn-outline-danger">
                                        <span class="fas fa-times"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </p>

        <p>
            <a href="{{ route('productos.create')}}" class="btn btn-outline-info"> 
                <span class="fas fa-plus"></span> Agregar nuevo producto
            </a>
            <a href="{{ route('productos.excel')}}" class="btn btn-outline-success"> 
                <span class="fas fa-file-excel"></span> Exportar Excel
            </a>

            <form action="productos/import" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="file" name="file" />
                    <button type="submit" class="btn btn-outline-warning"><span class="fas fa-file-excel"></span>Importar Excel</button>
                </div>
            </form>

            <br>
            <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
                <div class="card-header">Acerca de importar</div>
                <div class="card-body">
                  <p class="card-text">Al importar, el archivo debe tener las columnas:
                    nombre, descripcion, precio y cantidad.
                  </p>
                </div>
            </div>

        </p>
    </div>
    </div>

@endsection

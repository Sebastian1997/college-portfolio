<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{

    public function index()
    {
        //

    }


    public function create()
    {
        //
        return "aqui puedes agregar";
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Producto $producto)
    {
        //
    }

    public function edit(Producto $producto)
    {
        //
        return "aqui puedes editar";
    }


    public function update(Request $request, Producto $producto)
    {
        //
    }


    public function destroy(Producto $producto)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Producto;

class AdminController extends Controller
{
    //
    public function view_category(){
        $data=category::all();
        
        return view('admin.category', compact('data'));
    }

    public function add_category(Request $request){
        $data=new category;

        $data->category_name=$request->category;

        $data->save();

        return redirect()->back()->with('message', 'Category Added Succesfully');
    }

    public function delete_category($id){
        $data=category::find($id);

        $data->delete();

        return redirect()->back()->with('message', 'Category succesfully deleted');
        
    }

    public function view_product(){
        $category = category::all();
        return view('admin.product', compact('category'));
    }



    public function show_product(){
        
        $producto = producto::all();
        return view('admin.show_product', compact('producto'));
    
    }

    public function delete_product($id){
        $producto=producto::find($id);
        $producto->delete();

        return redirect()->back()->with('message', 'product deleted successfully');
    }

    public function update_product($id){

        $producto=producto::find($id);
        $category=category::all();

        return view('admin.update_product', compact('producto', 'category'));
    }

    public function update_product_confirm($id, Request $request){
        $producto=producto::find($id);

        $producto->title=$request->title;
        $producto->description=$request->description;
        $producto->price=$request->price;
        $producto->discount_price=$request->dis_price;
        $producto->category=$request->category;
        $producto->quantity=$request->quantity;

        $image = $request->image;

        if($image){

        $imagename = time().'.'.$image->getClientOriginalExtension();

        $request->image->move('product', $imagename);

        $producto->image=$imagename;

        }


        $producto->save();

        return redirect()->back()->with('message', 'Product Updated Successfully');

    }

    public function add_product(Request $request){
        $producto = new producto;

        $producto->title=$request->title;
        $producto->description=$request->description;
        $producto->price=$request->price;
        $producto->quantity=$request->quantity;
        $producto->discount_price=$request->dis_price;
        $producto->category=$request->category;
        
        

        $image=$request->image;



            $imagename=time().'.'.$image->getClientOriginalExtension();

            $request->image->move('product', $imagename);
    
            $producto->image=$imagename;


      

        $producto->save();


        return redirect()->back()->with('message', 'Product added with success');


    }

}

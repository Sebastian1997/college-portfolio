<!DOCTYPE html>
<html>
  <head>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">

      @include('admin.sidebar')
            <!-- partial:partials/_sidebar.html -->
      @include('admin.header')
        <!-- partial -->
        @include('admin.body')
    <!-- endinject -->
    <!-- Plugin js for this page -->
    @include('admin.script')
  </body>
</html>
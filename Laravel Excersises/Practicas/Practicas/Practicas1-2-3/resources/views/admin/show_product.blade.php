<!DOCTYPE html>
<html>
  <head>
    @include('admin.css')
    <style type="text/css">
    
        .center{
            margin: auto;
            border: 2px solid white;
            text-align: center;
            margin-top: 40px;
            padding-right: 5px;
            padding-left: 5px;
            width: 50%;
        }

        .font_size{
            text-align: center;
            font-size: 40px;
            padding-top: 20px;
        }
        .img_size{
            width: 150px;
            height: 150px;
        }

        .th_color{
            background: skyblue;
        }

        .th_deg{
            padding: 30px;
        }

    </style>
  </head>
  <body>
    <div class="container-scroller">

      @include('admin.sidebar')
            <!-- partial:partials/_sidebar.html -->
      @include('admin.header')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                @if(session()->has('message'))
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert"
                  aria-hidden="true">x</button>
                  {{session()->get('message')}}
                </div>
              @endif

                <h2 class="font_size">Todos los productos</h2>

                <table class="center">
                    <tr class="th_color">
                        <th class="th_deg">Nombre del producto</th>
                        <th class="th_deg">Descripcion</th>
                        <th class="th_deg">Cantidad</th>
                        <th class="th_deg">Categoria</th>
                        <th class="th_deg">Precio</th>
                        <th class="th_deg">Precio de descuento</th>
                        <th class="th_deg">Imagen del producto</th>
                        <th class="th_deg">Eliminar</th>
                        <th class="th_deg">Editar</th>
                    </tr>

                    @foreach($producto as $producto)
                    <tr>
                        <td>{{$producto->title}}</td>
                        <td>{{$producto->description}}</td>
                        <td>{{$producto->quantity}}</td>
                        <td>{{$producto->category}}</td>
                        <td>{{$producto->price}}</td>
                        <td>{{$producto->discount_price}}</td>
                        <td>

                            <img class="img_size" src="/product/{{$producto->image}}" alt="">
                        </td>

                        <td>
                            <a class="btn btn-danger" onclick="return confirm('Are you sure to delete this product')" 
                            href="{{url('delete_product', $producto->id)}}">Delete</a>
                        </td>

                        <td>
                            <a class="btn btn-success" href="{{url('update_product', $producto->id)}}">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    @include('admin.script')
  </body>
</html>
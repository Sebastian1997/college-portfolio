<!DOCTYPE html>
<html>
  <head>
    @include('admin.css')

    <style type="text/css">
    
    .div_center
    {
        text-align: center;
        padding-top:40px;
    }

    .font_size
    {
        font-size: 40px;
        padding-bottom: 40px;
    }

    .text_color{
        color: black;
        padding-bottom: 20px; 
    }

    label{
        display: inline-block;
        width: 200px;
    }

    .div_design{
        padding-bottom: 15px;
    }

    </style>
  </head>
  <body>
    <div class="container-scroller">

      @include('admin.sidebar')
            <!-- partial:partials/_sidebar.html -->
      @include('admin.header')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">


            @if(session()->has('message'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"
              aria-hidden="true">x</button>
              {{session()->get('message')}}
            </div>
            @endif

                <div class="div_center">
                    <h1 class="font_size">Añadir productos</h1>

                    <form action="{{url('/add_product')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                        <div class="div_design">
                    <label>Producto</label>
                    <input type="text" name="title" placeholder="Escribe el nombre del producto" 
                    required="" class="text_color">
                    </div>

                    <div class="div_design">
                        <label>Descripcion de producto</label>
                        <input type="text" name="description" placeholder="Escribe una descripcion" 
                        required="" class="text_color">
                    </div>

                    <div class="div_design">
                        <label>Precio</label>
                        <input type="number" name="price" placeholder="Escribe el precio" 
                        required="" class="text_color">
                    </div>


                    <div class="div_design">
                        <label>Precio con descuento</label>
                        <input type="number" name="dis_price" placeholder="Escribe el precio con descuento" 
                         class="text_color">
                    </div>

                    <div class="div_design">
                        <label>Cantidad de stock</label>
                        <input type="number" min="0" name="quantity" placeholder="Escribe una cantidad" 
                        required="" class="text_color">
                    </div>

                    <div class="div_design">
                        <label>Categoria</label>
                        <select class="text_color" name="category" required="">
                            <option value="" selected>Añade categoria aqui</option>
                            @foreach($category as $category)
                            <option value="{{$category->category_name}}" >{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="div_design">
                        <label>Product Image</label>

                        <input type="file" name="image" required="">
                    </div>


                    <div class="div_design">
                        <input type="submit" value="Add Product" class="btn btn-primary">
                    </div>

                </form>


                </div>

            </div>
        </div>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    @include('admin.script')
  </body>
</html>
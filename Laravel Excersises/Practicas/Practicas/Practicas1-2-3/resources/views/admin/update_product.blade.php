<!DOCTYPE html>
<html>
  <head>
    <base href="/public">

    @include('admin.css')

    <style type="text/css">
    
    .div_center
    {
        text-align: center;
        padding-top:40px;
    }

    .font_size
    {
        font-size: 40px;
        padding-bottom: 40px;
    }

    .text_color{
        color: black;
        padding-bottom: 20px; 
    }

    label{
        display: inline-block;
        width: 200px;
    }

    .div_design{
        padding-bottom: 15px;
    }

    </style>
  </head>
  <body>
    <div class="container-scroller">

      @include('admin.sidebar')
            <!-- partial:partials/_sidebar.html -->
      @include('admin.header')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">


            @if(session()->has('message'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"
              aria-hidden="true">x</button>
              {{session()->get('message')}}
            </div>
            @endif

                <div class="div_center">
                    <h1 class="font_size">Update product</h1>

                    <form action="{{url('/update_product_confirm', $producto->id)}}" method="POST" 
                    enctype="multipart/form-data">

                    @csrf
                    
                        <div class="div_design">
                    <label>Product Title</label>
                    <input type="text" name="title" placeholder="Write a title" 
                    required="" class="text_color" value="{{$producto->title}}">
                    </div>

                    <div class="div_design">
                        <label>Product Description</label>
                        <input type="text" name="description" placeholder="Write a description" 
                        required="" class="text_color" value="{{$producto->description}}">
                    </div>

                    <div class="div_design">
                        <label>Product Price</label>
                        <input type="number" name="price" placeholder="Write the price" 
                        required="" class="text_color" value="{{$producto->price}}">
                    </div>


                    <div class="div_design">
                        <label>Discount price</label>
                        <input type="number" name="dis_price" placeholder="Write a discount" 
                         class="text_color" value="{{$producto->discount_price}}">
                    </div>

                    <div class="div_design">
                        <label>Product Quantity</label>
                        <input type="number" min="0" name="quantity" placeholder="Write the quantity" 
                        required="" class="text_color" value="{{$producto->quantity}}">
                    </div>

                    <div class="div_design">
                        <label>Product Category</label>
                        <select class="text_color" name="category" required="">
                            <option value="{{$producto->category}}" selected="">
                                {{$producto->category}}</option>

                                @foreach($category as $category)
                                <option value="{{$category->category_name}}" >{{$category->category_name}}</option>
                                @endforeach
                   
                        </select>
                    </div>


                    <div class="div_design">
                        <label>Current Product Image</label>

                        <img style="margin:auto;" height="100" width="100" src="/product/{{$producto->image}}" alt="">
                    </div>


                    <div class="div_design">
                        <label>ChangeProduct Image</label>

                        <input type="file" name="image">
                    </div>


                    <div class="div_design">
                        <input type="submit" value="Update Product" class="btn btn-primary">
                    </div>

                </form>


                </div>

            </div>
        </div>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    @include('admin.script')
  </body>
</html>
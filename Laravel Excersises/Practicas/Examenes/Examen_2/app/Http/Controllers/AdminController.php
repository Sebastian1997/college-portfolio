<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class AdminController extends Controller
{
    //
    public function view_category(){

        $data=Category::all();
        return view('admin.category', compact('data'));
    }
    public function add_category(Request $request){
        $data=new category;

        $data->category_name=$request->category;

        $data->save();

        return redirect()->back()->with('message','Categoria añadida con exito');
    }

    public function delete_category($id){
        $data=Category::find($id);
        $data->delete();
        return redirect()->back()->with('message','Categoria eliminada con exito');
    }

    public function view_product(){
        $category = Category::all();
        return view('admin.product', compact('category'));
    }

    public function add_product(Request $request){
        $product = new Product;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount_price = $request->dis_price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category;

        $image=$request->image;
        $imagename=time().'.'.$image->getClientOriginalExtension();
        $request->image->move('product', $imagename);
        $product->image=$imagename;

        $product->save();

        return redirect()->back()->with('message','Producto añadido con exito');
    }

    public function show_product(){
        $products = Product::all();
        return view('admin.show_product', compact('products'));
    }

    public function delete_product($id){
        $data=Product::find($id);
        $data->delete();
        return redirect()->back()->with('messagedelete','Producto eliminado con exito');
    }

    public function update_product($id){
        $producto=Product::find($id);
        $category = Category::all();
        return view('admin.update_product', compact('producto','category'));
    }

    public function update_product_confirm(Request $request, $id){
    
        $product=Product::find($id);
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount_price = $request->dis_price; 
        $product->category_id = $request->category;
        $product->quantity = $request->quantity;

        if($request->image != null){
        $image=$request->image;
        $imagename=time().'.'.$image->getClientOriginalExtension();
        $request->image->move('product', $imagename);
        $product->image=$imagename;
        }

        $product->save();

        return redirect()->back()->with('messageupdate','Producto actualizado con exito');
     
    }
}


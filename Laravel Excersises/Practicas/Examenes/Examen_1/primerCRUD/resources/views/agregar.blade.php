@extends('layout/plantilla')

@section('tituloPagina', 'Crear un nuevo registro')

@section('contenido')

    <br>

    <div class="card">
    <div class="card-header">
      Featured
    </div>
    <div class="card-body">
      <h5 class="card-title">Agregar nuevo</h5>
      <p class="card-text">

        <form action="{{ route('productos.store')}}" method="POST">
            @csrf
            <label for="">Nombre</label>
            <input type="text" name="nombre" class="form-control required">
            <label for="">Descripcion</label>
            <input type="text" name="descripcion" class="form-control required">
            <label for="">Precio</label>
            <input type="text" name="precio" class="form-control required">
            <label for="">Cantidad</label>
            <input type="text" name="cantidad" class="form-control required">
            <br>
            <button class="btn btn-primary">Agregar</button>
            <a class="btn btn-info" href="{{ route("productos.index")}}">Regresar</a>
        </form>
      </p>
      
    </div>
  </div>

@endsection
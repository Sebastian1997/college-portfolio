package uia.arqsoft.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.exception.ResourceNotFoundException;
import uia.arqsoft.model.Contacto;
import uia.arqsoft.repositorio.ContactoRepositorio;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContactoServicioImpl implements ContactoServicio{

    @Autowired
    private ContactoRepositorio repositorio;
    @Override
    public Contacto crearContacto(Contacto contacto) {
        return repositorio.save(contacto);
    }

    @Override
    public Contacto updateContacto(Contacto contacto) {
        Optional<Contacto> contactoDb = this.repositorio.findById(contacto.getId());

        if(contactoDb.isPresent()){
            Contacto contactoUpdate = contactoDb.get();
            contactoUpdate.setId(contacto.getId());
            contactoUpdate.setNombre(contacto.getNombre());
            contactoUpdate.setApellido(contacto.getApellido());
            contactoUpdate.setTelefono(contacto.getTelefono());

            repositorio.save(contactoUpdate);
            return contactoUpdate;
        } else{
            throw new ResourceNotFoundException("No se encontro el contacto");
        }
    }

    @Override
    public List<Contacto> getAllContactos() {
        return this.repositorio.findAll();
    }

    @Override
    public Contacto getContactoById(Long id) {
        Optional<Contacto> contactoDb = this.repositorio.findById(id);

        if(contactoDb.isPresent()){
            return contactoDb.get();
        }else{
            throw new ResourceNotFoundException("No se encontro el contacto");
        }
    }

    @Override
    public void deleteContacto(Long id) {
        Optional<Contacto> contactoDb = this.repositorio.findById(id);

        if(contactoDb.isPresent()){
            this.repositorio.delete(contactoDb.get());
        }else{
            throw new ResourceNotFoundException("No se encontro el contacto");
        }
    }
}

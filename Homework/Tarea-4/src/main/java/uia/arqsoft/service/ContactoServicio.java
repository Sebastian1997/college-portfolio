package uia.arqsoft.service;

import uia.arqsoft.model.Contacto;

import java.util.List;

public interface ContactoServicio {
    Contacto crearContacto(Contacto contacto);
    Contacto updateContacto(Contacto contacto);
    List<Contacto> getAllContactos();
    Contacto getContactoById(Long id);
    void deleteContacto(Long id);
}

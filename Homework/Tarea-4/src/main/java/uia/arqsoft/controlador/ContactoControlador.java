package uia.arqsoft.controlador;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.model.Contacto;
import uia.arqsoft.service.ContactoServicio;

import java.util.List;

@RestController
public class ContactoControlador {
    @Autowired
    private ContactoServicio servicio;

    @GetMapping("/contactos")
    public ResponseEntity<List<Contacto>> getAllContactos(){
        return ResponseEntity.ok().body(servicio.getAllContactos());
    }

    @GetMapping("/contactos/{id}")
    public ResponseEntity<Contacto> getContactoById(@PathVariable Long id){
        return ResponseEntity.ok().body(servicio.getContactoById(id));
    }

    @PostMapping("/contactos")
    public ResponseEntity<Contacto> createContacto(@RequestBody Contacto contacto){
        return ResponseEntity.ok().body(this.servicio.crearContacto(contacto));
    }

    @PutMapping("/contactos/{id}")
    public ResponseEntity<Contacto> updateContacto(@PathVariable Long id, @RequestBody Contacto contacto){
        contacto.setId(id);
        return ResponseEntity.ok().body(this.servicio.updateContacto(contacto));
    }

    @DeleteMapping("/contactos/{id}")
    public HttpStatus deleteContacto(@PathVariable Long id){
        this.servicio.deleteContacto(id);
        return HttpStatus.OK;
    }
}

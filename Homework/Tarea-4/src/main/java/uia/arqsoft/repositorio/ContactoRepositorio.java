package uia.arqsoft.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.model.Contacto;



public interface ContactoRepositorio extends JpaRepository<Contacto, Long> {
}

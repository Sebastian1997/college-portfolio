package uia.arqsoft.model.dao;

import org.springframework.data.repository.CrudRepository;
import uia.arqsoft.model.entity.Productos;


public interface ProductosDao extends CrudRepository<Productos, Long> {
}

package uia.arqsoft.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.model.dao.ProductosDao;
import uia.arqsoft.model.entity.Productos;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class ProductosServiceImpl implements ProductosService{

    /*
    //Inyeccion por parámetro
    @Autowired
    private ProductosDao productosDao;

     */
    private ProductosDao productosDao;

    //Inyección por constructor
    @Autowired
    public ProductosServiceImpl(ProductosDao productosDao) {
        this.productosDao = productosDao;
    }

    @Override
    public List<Productos> getProductos() {
        return (List<Productos>) productosDao.findAll();
    }

    @Override
    public Optional<Productos> getProductoById(Long id) {
        return productosDao.findById(id);
    }

    @Override
    public void deleteById(Long id){
        productosDao.deleteById(id);
    }

    @Override
    public Productos save(Productos productos) {
        productosDao.save(productos);
        return productos;
    }
}

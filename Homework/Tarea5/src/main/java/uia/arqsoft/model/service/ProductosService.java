package uia.arqsoft.model.service;


import uia.arqsoft.model.entity.Productos;

import java.util.List;
import java.util.Optional;

public interface ProductosService {
    List<Productos> getProductos();
    Optional<Productos> getProductoById(Long id);

    void deleteById(Long id);

    Productos save(Productos productos);
}

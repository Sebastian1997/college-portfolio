package uia.arqsoft.model.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.model.entity.Productos;
import uia.arqsoft.model.service.ProductosService;
import uia.arqsoft.model.service.ProductosServiceImpl;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "coleccion")
public class ProductosController {
    private ProductosService service;


    //Inyeccion por setter
    @Autowired
    public void setService(ProductosService serv){
        this.service = serv;
    }


    @GetMapping(value = "productos")
    public List<Productos> getProductos(){
        return service.getProductos();
    }

    @GetMapping(value = "productos/{id}")
    public Optional<Productos> getProductoById(@PathVariable(name ="id") Long id){
        return service.getProductoById(id);
    }

    @DeleteMapping(value = "productos/{id}")
    public ResponseEntity<HttpStatus> deleteProductById(@PathVariable(name = "id") Long id){

        try{
            service.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/productos")
    public ResponseEntity<Productos> saveProducto(@RequestBody Productos productos){
        try{
            Productos product = service.save(new Productos(productos.getDescripcion(), productos.getNombre(), productos.getCantidad()));
            return new ResponseEntity<>(product, HttpStatus.CREATED);
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/productos/{id}")
    public ResponseEntity<Productos> update(@PathVariable(name = "id") Long id, @RequestBody Productos productos){

        Optional<Productos> productosData = service.getProductoById(id);
        if(productosData.isPresent()){
            Productos product = productosData.get();
            product.setDescripcion(productos.getDescripcion());
            product.setCantidad(productos.getCantidad());
            product.setNombre(productos.getNombre());
            return new ResponseEntity<>(service.save(product), HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

}

package uia.arqsoft;

import uia.arqsoft.database.*;

import java.sql.SQLData;

public class Main {
    public static void main(String[] args) {
        System.out.println("-----------------------------------------------------");
        System.out.println("                       DBMS Info   ");
        System.out.println("-----------------------------------------------------");
        MySql mySql = new MySql("Sebastian", "1997");
        Redis redis = new Redis("Rodrigo", "1998");
        PostgreSql postgreSql = new PostgreSql("Luis", "2000");
        InfoPrinter.getInfo(mySql);
        InfoPrinter.getInfo(postgreSql);
        InfoPrinter.getInfo(redis);

        System.out.println("");
        System.out.println("-----------------------------------------------------");
        System.out.println("                  Logs information   ");
        System.out.println("-----------------------------------------------------");

        Log.showLogs();
    }

}
package uia.arqsoft.database;

public class MySql extends Dbms{

    private String name;
    private String port;
    private String username;
    private String password;

    public MySql(String username, String password) {
        this.name = "MySQL";
        this.port = "3006";
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String tString() {
        return "MySql{" +
                "name='" + name + '\'' +
                ", port='" + port + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

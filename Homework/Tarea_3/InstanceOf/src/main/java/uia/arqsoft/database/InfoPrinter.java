package uia.arqsoft.database;

public class InfoPrinter {
    public static void getInfo(Dbms dbms){
        if(dbms instanceof Redis){
            String info = ((Redis) dbms).tString();
            System.out.println(info);
            Log.createLog("Checked info from: " + ((Redis) dbms).getUsername());
        }else if(dbms instanceof MySql){
            String info = ((MySql) dbms).tString();
            System.out.println(info);
            Log.createLog("Checked info from: " + ((MySql) dbms).getUsername());
        }else if(dbms instanceof PostgreSql){
            String info = ((PostgreSql) dbms).tString();
            System.out.println(info);
            Log.createLog("Checked info from: " + ((PostgreSql) dbms).getUsername());
        }

    }

}

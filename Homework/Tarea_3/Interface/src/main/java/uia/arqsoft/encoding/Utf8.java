package uia.arqsoft.encoding;

public class Utf8 implements IEncoding{
    @Override
    public void code() {
        System.out.println("Encoding with UTF8");
    }

    @Override
    public void decode() {
        System.out.println("Decding with UTF8");
    }
}

package uia.arqsoft.encoding;

public class EncodingFactory {
    public static IEncoding getEcoding(String type) throws NullPointerException{
        if(type.equalsIgnoreCase("UTF8")){
            return new Utf8();
        }else if (type.equalsIgnoreCase("ASCII")){
            return new Ascii();
        }else if (type.equalsIgnoreCase("UTF32")){
            return new Utf32();
        }

        return null;
    }

}

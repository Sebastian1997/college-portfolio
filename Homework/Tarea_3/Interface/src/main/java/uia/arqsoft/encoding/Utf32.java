package uia.arqsoft.encoding;

public class Utf32 implements IEncoding{
    @Override
    public void code() {
        System.out.println("Encoding with Utf32");
    }

    @Override
    public void decode() {
        System.out.println("Decoding with Utf32");
    }
}

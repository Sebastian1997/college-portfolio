package uia.arqsoft;

import uia.arqsoft.encoding.EncodingFactory;
import uia.arqsoft.encoding.IEncoding;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {

        LocalDate ld = LocalDate.now();
        System.out.println("Bienvenido, la fecha del dia de hoy es");
        System.out.println(ld);

        IEncoding[] iEncodingList = new IEncoding[3];
        iEncodingList[0]=EncodingFactory.getEcoding("UTF8");
        iEncodingList[1]=EncodingFactory.getEcoding("UTF32");
        iEncodingList[2]=EncodingFactory.getEcoding("ASCII");


        printInfo(iEncodingList);

    }


    public static void printInfo(IEncoding ... listEcoding){
        System.out.println("--------------------------------------");
        LocalTime lt = LocalTime.now();
        System.out.println("Process started at " + lt.toString());
        for (IEncoding i : listEcoding){
            try {
                i.decode();
                i.code();
            }catch (Exception e){
                System.out.println("No existe el tipo seleccionado");
            }
        }
        System.out.println("Process finished at " + lt.toString());
        System.out.println("--------------------------------------");
    }

}


package uia.arqsoft.encoding;

import java.time.LocalDate;
import java.time.LocalTime;

public class Ascii implements IEncoding{
    @Override
    public void code() {
        System.out.println("Encoding ascii");

    }

    @Override
    public void decode() {
        System.out.println("Decoding ascii");
    }

}

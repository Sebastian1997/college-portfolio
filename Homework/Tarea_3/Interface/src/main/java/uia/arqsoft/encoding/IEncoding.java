package uia.arqsoft.encoding;

public interface IEncoding {
    void code();
    void decode();

}

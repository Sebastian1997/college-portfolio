package uia.arqsoft.micros3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.micros3.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}

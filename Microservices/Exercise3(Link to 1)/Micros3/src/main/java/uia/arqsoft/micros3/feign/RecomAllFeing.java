package uia.arqsoft.micros3.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import uia.arqsoft.micros3.dtos.RecomendacionDTO;

import java.util.List;

@FeignClient(value = "RecomAllFeign", url = "http://localhost:8081", path = "/recomendaciones")
public interface RecomAllFeing {

    @GetMapping()
    List<RecomendacionDTO> getRecomendaciones();
}

package uia.arqsoft.micros3.dtos;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserDto {

    private Integer id;

    private String name;

    private String email;

    @JsonIgnore
    private String password;
}

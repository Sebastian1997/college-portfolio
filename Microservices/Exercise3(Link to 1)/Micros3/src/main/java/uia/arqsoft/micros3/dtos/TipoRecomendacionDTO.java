package uia.arqsoft.micros3.dtos;

import lombok.*;

    @Getter
    @Setter
    @EqualsAndHashCode
    @AllArgsConstructor
    @NoArgsConstructor

    public class TipoRecomendacionDTO {

        private Integer id;
        private String nombre;
    }


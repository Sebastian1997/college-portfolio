package uia.arqsoft.micros3.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.micros3.dtos.UserDto;
import uia.arqsoft.micros3.models.User;
import uia.arqsoft.micros3.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    ObjectMapper mapper = new ObjectMapper();

    public List<UserDto> getUsuarios() {
        List<User> usuarios = userRepository.findAll();
        return usuarios.stream()
                .map(usuario -> mapper.convertValue(usuario, UserDto.class))
                .collect(Collectors.toList());
    }

    public UserDto getUsuarioById(Integer id) {
        User usuario = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id)));
        UserDto usuarioDto = mapper.convertValue(usuario, UserDto.class);
        return usuarioDto;
    }

    public UserDto createUsuario(UserDto usuario) {
        User usuarioToCreate = mapper.convertValue(usuario, User.class);
        return mapper.convertValue(userRepository.save(usuarioToCreate), UserDto.class);
    }

    public UserDto updateUsuario(Integer id, UserDto usuario) {
        User usuarioToUpdate = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id)));
        usuarioToUpdate.setName(usuario.getName());
        usuarioToUpdate.setEmail(usuario.getEmail());
        usuarioToUpdate.setPassword(usuario.getPassword());
        return mapper.convertValue(userRepository.save(usuarioToUpdate), UserDto.class);
    }

    public void deleteUsuario(Integer id) {
        User usuario = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Usuario con id %d no encontrado", id)));
        userRepository.delete(usuario);
    }





}

package uia.arqsoft.micros3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Micros3Application {

    public static void main(String[] args) {
        SpringApplication.run(Micros3Application.class, args);
    }

}

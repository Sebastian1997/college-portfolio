package uia.arqsoft.micros3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.micros3.dtos.RecomendacionDTO;
import uia.arqsoft.micros3.dtos.UserDto;
import uia.arqsoft.micros3.feign.RecomAllFeing;
import uia.arqsoft.micros3.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioRestController {
    @Autowired
    private UserService userService;

    @Autowired
    private RecomAllFeing feign;

    @GetMapping("/rec")
    public ResponseEntity<List<RecomendacionDTO>> getRecomendaciones() {
        return new ResponseEntity<>(feign.getRecomendaciones(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getUsuarios() {
        return new ResponseEntity<>(userService.getUsuarios(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUsuario(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(userService.getUsuarioById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDto> createUsuario(@RequestBody UserDto user) {
        return new ResponseEntity<>(userService.createUsuario(user), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUsuario(@PathVariable("id") Integer id, @RequestBody UserDto user) {
        return new ResponseEntity<>(userService.updateUsuario(id, user), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteUsuario(@PathVariable("id") Integer id) {
        userService.deleteUsuario(id);
    }

}

package uia.arqsoft.micros3.dtos;

import lombok.*;
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RecomendacionDTO {
    private Integer id;

    private String nombre;

    private String sitioWeb;

    private String descripcion;

    private TipoRecomendacionDTO tipoRecomendacion;


}
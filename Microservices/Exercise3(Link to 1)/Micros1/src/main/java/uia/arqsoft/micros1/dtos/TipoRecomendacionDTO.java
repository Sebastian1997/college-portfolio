package uia.arqsoft.micros1.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

    @Getter
    @Setter
    @EqualsAndHashCode
    @AllArgsConstructor
    @NoArgsConstructor

    public class TipoRecomendacionDTO {

        private Integer id;
        private String nombre;
    }


package uia.arqsoft.micros1.repositories;

import uia.arqsoft.micros1.models.Recomendacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecomendacionRepository extends JpaRepository<Recomendacion, Integer> {

}

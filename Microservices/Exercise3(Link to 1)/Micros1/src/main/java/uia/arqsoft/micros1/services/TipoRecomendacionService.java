package uia.arqsoft.micros1.services;

import aj.org.objectweb.asm.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import uia.arqsoft.micros1.dtos.TipoRecomendacionDTO;
import uia.arqsoft.micros1.models.TipoRecomendacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.micros1.repositories.TipoRecomendacionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TipoRecomendacionService {
    @Autowired
    private TipoRecomendacionRepository tipoRecomendacionRepository;


    /*
    public List<TipoRecomendacion> getTiposRecomendacion() {

        return tipoRecomendacionRepository.findAll();
    }

     */
    ObjectMapper objectMapper = new ObjectMapper();
    public List<TipoRecomendacionDTO> getTiposRecomendacionDTO() {
        List<TipoRecomendacion> tiposRecomendacion = tipoRecomendacionRepository.findAll();
        return tiposRecomendacion.stream()
                .map(tipoRecomendacion -> objectMapper.convertValue(tipoRecomendacion, TipoRecomendacionDTO.class))
                .collect(Collectors.toList());
    }

    public TipoRecomendacionDTO getTipoRecomendacionById(Integer id) {
        TipoRecomendacion tipoRec = tipoRecomendacionRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Tipo de recomendación con id %d no encontrado", id)));
        TipoRecomendacionDTO tipoRecDTO = objectMapper.convertValue(tipoRec, TipoRecomendacionDTO.class);
        return tipoRecDTO;
    }


    public TipoRecomendacionDTO createTipoRecomendacion(TipoRecomendacionDTO tipo) {
        TipoRecomendacion tipoRecomendacionToCreate = objectMapper.convertValue(tipo, TipoRecomendacion.class);
        return objectMapper.convertValue(tipoRecomendacionRepository.save(tipoRecomendacionToCreate), TipoRecomendacionDTO.class);
    }

    public TipoRecomendacionDTO updateTipoRecomendacion(Integer id, TipoRecomendacionDTO tipoRecomendacion) {
        Optional<TipoRecomendacion> result = tipoRecomendacionRepository.findById(id);
        if (result.isPresent()) {
            TipoRecomendacion tipoRecomendacionToUpdate = result.get();
            tipoRecomendacionToUpdate.setNombre(tipoRecomendacion.getNombre());
            return objectMapper.convertValue(tipoRecomendacionRepository.save(tipoRecomendacionToUpdate), TipoRecomendacionDTO.class);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Tipo de recomendación con id %d no encontrado", id));
        }
    }

    public void deleteTipoRecomendacion(Integer id) {
        try {
            tipoRecomendacionRepository.deleteById(id);
        } catch (HttpStatusCodeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Tipo de recomendación con id %d no encontrado", id));
        }
    }

}

package uia.arqsoft.micros1.controllers;

import org.springframework.web.bind.annotation.*;
import uia.arqsoft.micros1.dtos.TipoRecomendacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import uia.arqsoft.micros1.services.TipoRecomendacionService;

import java.util.List;

@RestController
@RequestMapping("/tipo-recomendacion")
public class TipoRecomendacionController {
    @Autowired
    private TipoRecomendacionService tipoRecomendacionService;


    @GetMapping(value = "/all")
    public ResponseEntity<List<TipoRecomendacionDTO>> getTiposRecomendacion() {
        return new ResponseEntity(tipoRecomendacionService.getTiposRecomendacionDTO(), HttpStatus.OK);
    }

    @GetMapping(value = "/{tipoRecomendacionId}")
    public ResponseEntity<TipoRecomendacionDTO> getTipoRecomendacion(@PathVariable("tipoRecomendacionId") Integer tipoRecomendacionId) {
        return new ResponseEntity<>(tipoRecomendacionService.getTipoRecomendacionById(tipoRecomendacionId), HttpStatus.OK);
    }

    @PostMapping(value = "/new")
    public ResponseEntity<TipoRecomendacionDTO> createTipoRecomendacion(@RequestBody TipoRecomendacionDTO tipoRecomendacion) {
        return new ResponseEntity<TipoRecomendacionDTO>(tipoRecomendacionService.createTipoRecomendacion(tipoRecomendacion), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{tipoRecomendacionId}")
    public ResponseEntity<TipoRecomendacionDTO> updateTipoRecomendacion(@PathVariable("tipoRecomendacionId") Integer tipoRecomendacionId, @RequestBody TipoRecomendacionDTO tipoRecomendacion) {
        return new ResponseEntity<>(tipoRecomendacionService.updateTipoRecomendacion(tipoRecomendacionId, tipoRecomendacion), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{tipoRecomendacionId}")
    public void deleteTipoRecomendacion(@PathVariable("tipoRecomendacionId") Integer tipoRecomendacionId) {
        tipoRecomendacionService.deleteTipoRecomendacion(tipoRecomendacionId);
    }
}

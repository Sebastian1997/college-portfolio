package uia.arqsoft.micros1.models;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "recomendacion")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Recomendacion {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "sitio_web")
    private String sitioWeb;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne
    private TipoRecomendacion tipoRecomendacion;


}

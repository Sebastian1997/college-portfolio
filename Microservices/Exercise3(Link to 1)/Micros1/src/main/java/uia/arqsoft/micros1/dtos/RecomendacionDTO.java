package uia.arqsoft.micros1.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import uia.arqsoft.micros1.models.TipoRecomendacion;
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RecomendacionDTO {
    private Integer id;

    private String nombre;

    private String sitioWeb;

    private String descripcion;

    private TipoRecomendacion tipoRecomendacion;


}
package uia.arqsoft.micros1.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.micros1.dtos.RecomendacionDTO;
import uia.arqsoft.micros1.models.Recomendacion;
import uia.arqsoft.micros1.models.TipoRecomendacion;
import uia.arqsoft.micros1.repositories.RecomendacionRepository;
import uia.arqsoft.micros1.repositories.TipoRecomendacionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*
*
* DTO UTILIZANDO MÉTODO MANUAL
* */
@Service
public class RecomendacionService {
    @Autowired
    private RecomendacionRepository recomendacionRepository;

    @Autowired
    private TipoRecomendacionRepository tipoRecomendacionRepository;

    public List<RecomendacionDTO> getRecomendaciones() {
        List<Recomendacion> recomendaciones = recomendacionRepository.findAll();
        return recomendaciones.stream().map(recomendacion -> dtoConverter(recomendacion)).collect(Collectors.toList());
    }

    public RecomendacionDTO createRecomendacion(Integer tipoId, RecomendacionDTO recomendacion) {
        Optional<TipoRecomendacion> result = tipoRecomendacionRepository.findById(tipoId);

        if (result.isPresent()) {
            TipoRecomendacion tipoRecomendacion = result.get();
            recomendacion.setTipoRecomendacion(tipoRecomendacion);
            Recomendacion recomendacionToSave = this.entityConverter(recomendacion);
            recomendacionRepository.save(recomendacionToSave);
            return this.dtoConverter(recomendacionToSave);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Tipo de recomendación con id %d no encontrado", tipoId));
        }

    }
    public Recomendacion getRecomendacionById(Integer id) {
        return recomendacionRepository.findById(id).orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Recomendación con id %d no encontrada", id)));
    }

    public RecomendacionDTO updateRecomendacion(Integer id, RecomendacionDTO recomendacion) {
        Optional<Recomendacion> result = recomendacionRepository.findById(id);
        if (result.isPresent()) {
            Recomendacion recomendacionToUpdate = result.get();
            recomendacionToUpdate.setNombre(recomendacion.getNombre());
            recomendacionToUpdate.setDescripcion(recomendacion.getDescripcion());
            return this.dtoConverter(recomendacionRepository.save(recomendacionToUpdate));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Recomendación con id %d no encontrada", recomendacion.getId()));
        }
    }

    public void deleteRecomendacion(Integer id) {
        try {
            recomendacionRepository.deleteById(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Recomendación con id %d no encontrada", id));
        }
    }


    public RecomendacionDTO dtoConverter (Recomendacion recomendacion) {
        RecomendacionDTO recomendacionDTO = new RecomendacionDTO();
        recomendacionDTO.setId(recomendacion.getId());
        recomendacionDTO.setNombre(recomendacion.getNombre());
        recomendacionDTO.setDescripcion(recomendacion.getDescripcion());
        recomendacionDTO.setSitioWeb(recomendacion.getSitioWeb());
        recomendacionDTO.setTipoRecomendacion(recomendacion.getTipoRecomendacion());
        return recomendacionDTO;
    }

    public Recomendacion entityConverter (RecomendacionDTO recomendacionDTO) {
        Recomendacion recomendacion = new Recomendacion();
        recomendacion.setId(recomendacionDTO.getId());
        recomendacion.setNombre(recomendacionDTO.getNombre());
        recomendacion.setDescripcion(recomendacionDTO.getDescripcion());
        recomendacion.setSitioWeb(recomendacionDTO.getSitioWeb());
        recomendacion.setTipoRecomendacion(recomendacionDTO.getTipoRecomendacion());
        return recomendacion;
    }



}

package uia.arqsoft.micros1.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "tipo_recomendacion")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class TipoRecomendacion {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre")
    private String nombre;


}

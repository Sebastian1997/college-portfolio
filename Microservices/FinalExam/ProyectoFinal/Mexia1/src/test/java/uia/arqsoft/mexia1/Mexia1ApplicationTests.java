package uia.arqsoft.mexia1;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import uia.arqsoft.mexia1.models.Type;
import uia.arqsoft.mexia1.repositories.TypeRepository;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class Mexia1ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TypeRepository typeRepository;

    @Test
    public void testGetTypeById() throws Exception {
        Type type = new Type();
        type.setName("Test Type");


        Type savedType = typeRepository.save(type);


        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/types/" + savedType.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // verificar que el tipo devuelto por la API es el mismo que el que se guardó
        String responseBody = result.getResponse().getContentAsString();
        assert (responseBody.contains(savedType.getName()));
    }



}

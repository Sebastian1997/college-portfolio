package uia.arqsoft.mexia1.dtos;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class TypeDto {

    private Integer id;

    private String name;
}

package uia.arqsoft.mexia1.models;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "products")

//Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class Product {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Integer price;

    @Column(name = "stock")
    private Integer stock;

    @Column(name = "units")
    private Integer units;

    @Column(name = "age_restriction")
    private Integer ageRestriction;

    @ManyToOne
    @JoinColumn(name = "id_type")
    private Type type;

    public static class Builder {
        private String name;
        private String description;
        private Integer price;
        private Integer stock;
        private Integer units;


        public Builder(String name)
        {
            this.name = name;
        }

        public Builder description(String description)
        {
            this.description = description;
            return this;
        }

        public Builder price(Integer price)
        {
            this.price = price;
            return this;
        }

        public Builder stock(Integer stock)
        {
            this.stock = stock;
            return this;
        }

        public Builder units(Integer units)
        {
            this.units = units;
            return this;
        }


        public Product build()
        {
            return new Product(this);
        }
    }

    private Product(Builder builder)
    {
        this.name = builder.name;
        this.description = builder.description;
        this.price = builder.price;
        this.stock = builder.stock;
        this.units = builder.units;
    }


}

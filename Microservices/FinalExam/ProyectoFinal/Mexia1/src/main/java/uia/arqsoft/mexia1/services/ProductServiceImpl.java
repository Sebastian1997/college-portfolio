package uia.arqsoft.mexia1.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia1.dtos.ProductDto;
import uia.arqsoft.mexia1.models.Product;
import uia.arqsoft.mexia1.models.Type;
import uia.arqsoft.mexia1.repositories.ProductRepository;
import uia.arqsoft.mexia1.repositories.TypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl {
    private ProductRepository productRepository;
    private TypeRepository typeRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @Autowired
    public void setProductRepository(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    ObjectMapper mapper = new ObjectMapper();

    public List<ProductDto> getProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(product -> mapper.convertValue(product, ProductDto.class))
                .collect(Collectors.toList());
    }

    public ProductDto getProductById(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Producto con id %d no encontrado", id)));
        ProductDto productDto = mapper.convertValue(product, ProductDto.class);
        return productDto;
    }

    public ProductDto create(Integer typeId, ProductDto product) {
        Type type = typeRepository.findById(typeId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", typeId)));

        Product productToCreate;
        if (product.getUnits() == null){
             productToCreate = new Product.Builder(product.getName())
                    .description(product.getDescription())
                    .price(product.getPrice())
                    .stock(product.getStock())
                    .build();

             if(product.getAgeRestriction() != null) {productToCreate.setAgeRestriction(product.getAgeRestriction());}
             else {productToCreate.setAgeRestriction(0);}

             productToCreate.setType(type);

        }

        else{
            productToCreate = new Product.Builder(product.getName())
                    .description(product.getDescription())
                    .price(product.getPrice())
                    .stock(product.getStock())
                    .units(product.getUnits())
                    .build();

            if(product.getAgeRestriction() != null) {productToCreate.setAgeRestriction(product.getAgeRestriction());}
            else {productToCreate.setAgeRestriction(0);}

            productToCreate.setType(type);
        }

        return mapper.convertValue(productRepository.save(productToCreate), ProductDto.class);

    }

    public ProductDto update(Integer id, ProductDto product) {
        Product productToUpdate = productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Producto con id %d no encontrado", id)));
        productToUpdate.setName(product.getName());
        productToUpdate.setDescription(product.getDescription());
        productToUpdate.setPrice(product.getPrice());
        productToUpdate.setStock(product.getStock());
        productToUpdate.setUnits(product.getUnits());
        productToUpdate.setAgeRestriction(product.getAgeRestriction());
        return mapper.convertValue(productRepository.save(productToUpdate), ProductDto.class);
    }

    public void delete(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Producto con id %d no encontrado", id)));
        productRepository.delete(product);
    }

}

package uia.arqsoft.mexia1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mexia1Application {

    public static void main(String[] args) {
        SpringApplication.run(Mexia1Application.class, args);
    }

}

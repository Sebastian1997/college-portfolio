package uia.arqsoft.mexia1.dtos;


import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Integer id;
    private String name;
    private String description;
    private Integer price;
    private Integer stock;
    private Integer units;
    private TypeDto type;
    private Integer ageRestriction;

}

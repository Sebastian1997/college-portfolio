package uia.arqsoft.mexia1.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia1.dtos.TypeDto;
import uia.arqsoft.mexia1.models.Type;
import uia.arqsoft.mexia1.repositories.TypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeServiceImpl {

    private TypeRepository typeRepository;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public TypeServiceImpl(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }


    public List<TypeDto> findAll() {
        List<Type> types = typeRepository.findAll();
        return types.stream()
                .map(type -> mapper.convertValue(type, TypeDto.class))
                .collect(Collectors.toList());
    }

    public TypeDto findById(Integer id) {
        Type type = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        TypeDto typeDto = mapper.convertValue(type, TypeDto.class);
        return typeDto;
    }

    public TypeDto create(TypeDto type) {
        Type typeToCreate = mapper.convertValue(type, Type.class);
        return mapper.convertValue(typeRepository.save(typeToCreate), TypeDto.class);
    }

    public TypeDto update(Integer id, TypeDto type) {
        Type typeToUpdate = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        typeToUpdate.setName(type.getName());
        return mapper.convertValue(typeRepository.save(typeToUpdate), TypeDto.class);
    }

    public void delete(Integer id) {
        Type type = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        typeRepository.delete(type);
    }

}

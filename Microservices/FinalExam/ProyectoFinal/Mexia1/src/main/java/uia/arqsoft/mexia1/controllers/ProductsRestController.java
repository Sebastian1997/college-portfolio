package uia.arqsoft.mexia1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.mexia1.dtos.ProductDto;
import uia.arqsoft.mexia1.services.ProductServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsRestController {

    @Autowired
    private ProductServiceImpl productService;

    @GetMapping
    public ResponseEntity<List<ProductDto>> getProducts() {
        return new ResponseEntity<>(productService.getProducts(), org.springframework.http.HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable(name = "id") Integer id) {
        return new ResponseEntity<>(productService.getProductById(id), org.springframework.http.HttpStatus.OK);
    }

    @PostMapping("{typeid}")
    public ResponseEntity<ProductDto> createProduct(@PathVariable(name = "typeid") Integer typeid, @RequestBody ProductDto product) {
        return new ResponseEntity<>(productService.create(typeid, product), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable(name = "id") Integer id, @RequestBody ProductDto product) {
        return new ResponseEntity<>(productService.update(id, product), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable(name = "id") Integer id) {
        productService.delete(id);
    }





}

package uia.arqsoft.mexia1.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "types")

//Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;
}

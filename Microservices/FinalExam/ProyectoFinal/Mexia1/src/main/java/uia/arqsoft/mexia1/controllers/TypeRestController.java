package uia.arqsoft.mexia1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.mexia1.dtos.TypeDto;
import uia.arqsoft.mexia1.services.TypeServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/types")
public class TypeRestController {

    private TypeServiceImpl typeService;

    @Autowired
    public void setTypeService(TypeServiceImpl typeService) {
        this.typeService = typeService;
    }


    @GetMapping
    public ResponseEntity<List<TypeDto>> getTypes() {
        return new ResponseEntity<>(typeService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TypeDto> getTypeById(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(typeService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TypeDto> createType(@RequestBody TypeDto type) {
        return new ResponseEntity<>(typeService.create(type), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TypeDto> updateType(@PathVariable("id") Integer id, @RequestBody TypeDto type) {
        return new ResponseEntity<>(typeService.update(id, type), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteType(@PathVariable("id") Integer id) {
        typeService.delete(id);
    }

}

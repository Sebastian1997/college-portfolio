package uia.arqsoft.mexia2;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import uia.arqsoft.mexia2.abstractfactory.factory.MayorService;
import uia.arqsoft.mexia2.abstractfactory.factory.Servicio;
import uia.arqsoft.mexia2.abstractfactory.product.Bienvenida;
import uia.arqsoft.mexia2.abstractfactory.product.BienvenidaMayor;

import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Mexia2ApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void test_mayor() {
        Servicio servicioMayor = new MayorService();
        String bienvenida = servicioMayor.getBienvenida();
        String mensaje;

        Date date = Date.from(Instant.now());

        if (date.getHours() >= 12 && date.getHours() <= 18) {
            mensaje = "Buenos dias, ";
        } else if (date.getHours() >= 19 && date.getHours() <= 23) {
            mensaje = "Buenas tardes, ";
        } else {
            mensaje = "Buenas noches, ";

            assertEquals(mensaje, bienvenida);

        }
    }
}

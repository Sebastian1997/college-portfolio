package uia.arqsoft.mexia2.dtos;

import lombok.*;

//Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class UserDto {
    private Integer id;

    private String name;

    private String lastName;

    private Integer age;

    private String email;

    private String password;

    private TypeDto type;

}

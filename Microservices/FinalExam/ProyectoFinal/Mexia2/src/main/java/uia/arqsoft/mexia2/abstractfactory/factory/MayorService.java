package uia.arqsoft.mexia2.abstractfactory.factory;

import uia.arqsoft.mexia2.abstractfactory.product.*;
import uia.arqsoft.mexia2.feign.ProductsFeign;

import java.time.Instant;
import java.util.Date;

public class MayorService extends Servicio{

    @Override
    public Catalogos getCatalogo(ProductsFeign productsFeign) {
        CatalogoMayor catalogoMayor = new CatalogoMayor();
        catalogoMayor.setProductsFeign(productsFeign);
        return catalogoMayor;
    }

    @Override
    public String getBienvenida() {
        Date date =  Date.from(Instant.now());
        System.out.println(date);
        Bienvenida bienvenida = new BienvenidaMayor();

        if (date.getHours() >= 12 && date.getHours() <= 18) {
            return bienvenida.bienvenidaTarde();
        } else if (date.getHours() >= 19 && date.getHours() <= 23) {
            return bienvenida.bienvenidaNoche();
        } else {
            return bienvenida.bienvenidaMañana();
        }
    }
}

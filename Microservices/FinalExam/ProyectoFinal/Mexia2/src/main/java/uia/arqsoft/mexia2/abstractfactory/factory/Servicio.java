package uia.arqsoft.mexia2.abstractfactory.factory;

import uia.arqsoft.mexia2.abstractfactory.product.Bienvenida;
import uia.arqsoft.mexia2.abstractfactory.product.Catalogos;
import uia.arqsoft.mexia2.feign.ProductsFeign;

public abstract class Servicio {
    public abstract Catalogos getCatalogo(ProductsFeign productsFeign);

    public abstract String getBienvenida();
}

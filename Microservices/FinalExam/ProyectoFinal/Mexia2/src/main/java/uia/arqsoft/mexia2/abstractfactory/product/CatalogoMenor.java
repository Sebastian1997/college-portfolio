package uia.arqsoft.mexia2.abstractfactory.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.mexia2.dtos.ProductDto;
import uia.arqsoft.mexia2.feign.ProductsFeign;

import java.util.List;

@Service
public class CatalogoMenor implements Catalogos{

    private ProductsFeign productsFeign;

    public CatalogoMenor() {
    }

    @Autowired
    public void setProductsFeign(ProductsFeign productsFeign) {
        this.productsFeign = productsFeign;
    }

    @Override
    public List<ProductDto> getCatalogo() {
        List<ProductDto> products = productsFeign.getProducts();
        products.removeIf(productDto -> productDto.getAgeRestriction() == 1);
        return products;
    }
}

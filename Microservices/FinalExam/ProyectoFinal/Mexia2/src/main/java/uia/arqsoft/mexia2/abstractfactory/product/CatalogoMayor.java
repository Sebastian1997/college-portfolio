package uia.arqsoft.mexia2.abstractfactory.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.mexia2.dtos.ProductDto;
import uia.arqsoft.mexia2.feign.ProductsFeign;

import java.util.List;

@Service
public class CatalogoMayor implements Catalogos {

    private ProductsFeign productsFeign;

    public CatalogoMayor() {
    }

    @Autowired
    public void setProductsFeign(ProductsFeign productsFeign) {
        this.productsFeign = productsFeign;
    }
    @Override
    public List<ProductDto> getCatalogo() {
        return productsFeign.getProducts();
    }
}

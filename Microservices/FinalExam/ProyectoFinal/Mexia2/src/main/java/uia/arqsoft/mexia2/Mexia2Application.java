package uia.arqsoft.mexia2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Mexia2Application {

    public static void main(String[] args) {
        SpringApplication.run(Mexia2Application.class, args);
    }

}

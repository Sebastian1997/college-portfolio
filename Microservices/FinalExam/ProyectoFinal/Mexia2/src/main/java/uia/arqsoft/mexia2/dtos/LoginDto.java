package uia.arqsoft.mexia2.dtos;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {
    private String name;
    private String password;

}

package uia.arqsoft.mexia2.dtos;


import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Integer id;
    private String name;
    private String description;
    private Integer price;
    private Integer stock;
    private Integer units;
    private Integer ageRestriction;
    private TypeDto type;

}

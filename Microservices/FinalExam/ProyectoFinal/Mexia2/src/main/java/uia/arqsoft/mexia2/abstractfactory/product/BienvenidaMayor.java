package uia.arqsoft.mexia2.abstractfactory.product;

public class BienvenidaMayor implements Bienvenida{
    @Override
    public String bienvenidaMañana() {
        return "Buenos dias, ";
    }

    @Override
    public String bienvenidaTarde() {
        return "Buenas tardes, ";
    }

    @Override
    public String bienvenidaNoche() {
        return "Buenas noches, ";
    }
}

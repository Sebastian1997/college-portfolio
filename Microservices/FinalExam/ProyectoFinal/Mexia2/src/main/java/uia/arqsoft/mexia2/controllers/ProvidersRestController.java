package uia.arqsoft.mexia2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uia.arqsoft.mexia2.dtos.ProviderDto;
import uia.arqsoft.mexia2.feign.ProvidersFeign;

import java.util.List;

@RestController
@RequestMapping("/providers")
public class ProvidersRestController {

    @Autowired
    private ProvidersFeign providersFeign;

    @GetMapping
    public List<ProviderDto> getProviders() {
        return providersFeign.getProviders();
    }
}

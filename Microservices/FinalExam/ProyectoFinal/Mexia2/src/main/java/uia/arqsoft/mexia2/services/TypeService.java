package uia.arqsoft.mexia2.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia2.dtos.TypeDto;
import uia.arqsoft.mexia2.models.Type;
import uia.arqsoft.mexia2.repositories.TypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeService {
    private TypeRepository typeRepository;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setTypeRepository(TypeRepository typeRepository){
        this.typeRepository = typeRepository;
    }

    public List<TypeDto> getTypes() {
        List<Type> types = typeRepository.findAll();
        return types.stream()
                .map(type -> mapper.convertValue(type, TypeDto.class))
                .collect(Collectors.toList());
    }

    public TypeDto getTypeById(Integer id) {
        Type type = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        TypeDto typeDto = mapper.convertValue(type, TypeDto.class);
        return typeDto;
    }

    public TypeDto create(TypeDto type) {
        Type typeToCreate = mapper.convertValue(type, Type.class);
        Type typeCreated = typeRepository.save(typeToCreate);
        return mapper.convertValue(typeCreated, TypeDto.class);
    }

    public TypeDto updateType (Integer id, TypeDto type) {
        Type typeToUpdate = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        typeToUpdate.setName(type.getName());
        return mapper.convertValue(typeRepository.save(typeToUpdate), TypeDto.class);
    }


    public void deleteType(Integer id) {
        Type type = typeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", id)));
        typeRepository.delete(type);
    }



}

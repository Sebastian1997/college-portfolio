package uia.arqsoft.mexia2.abstractfactory.factory;

import uia.arqsoft.mexia2.abstractfactory.product.Bienvenida;
import uia.arqsoft.mexia2.abstractfactory.product.BienvenidaMenor;
import uia.arqsoft.mexia2.abstractfactory.product.CatalogoMenor;
import uia.arqsoft.mexia2.abstractfactory.product.Catalogos;
import uia.arqsoft.mexia2.feign.ProductsFeign;

import java.time.Instant;
import java.util.Date;

public class MenorService extends Servicio {
    @Override
    public Catalogos getCatalogo(ProductsFeign productsFeign) {
        CatalogoMenor catalogoMenor = new CatalogoMenor();
        catalogoMenor.setProductsFeign(productsFeign);
        return catalogoMenor;
    }

    @Override
    public String getBienvenida() {
        Date date = Date.from(Instant.now());
        System.out.println(date);
        Bienvenida bienvenida = new BienvenidaMenor();

        if (date.getHours() >= 12 && date.getHours() <= 18) {
            return bienvenida.bienvenidaTarde();
        } else if (date.getHours() >= 19 && date.getHours() <= 23) {
            return bienvenida.bienvenidaNoche();
        } else {
            return bienvenida.bienvenidaMañana();
        }
    }

}


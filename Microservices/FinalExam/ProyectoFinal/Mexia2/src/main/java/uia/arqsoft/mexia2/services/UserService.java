package uia.arqsoft.mexia2.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia2.dtos.UserDto;
import uia.arqsoft.mexia2.models.Type;
import uia.arqsoft.mexia2.models.User;
import uia.arqsoft.mexia2.repositories.TypeRepository;
import uia.arqsoft.mexia2.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    private TypeRepository typeRepository;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public UserService(TypeRepository typeRepository){
        this.typeRepository = typeRepository;
    }

    public List<UserDto> getUsers(){
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(user -> mapper.convertValue(user, UserDto.class))
                .collect(Collectors.toList());
    }

    public UserDto getUserById(Integer id){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User con id %d no encontrado", id)));
        UserDto userDto = mapper.convertValue(user, UserDto.class);
        return userDto;

    }

    public UserDto createUser(Integer typeId, UserDto user){
        Type type = typeRepository.findById(typeId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Type con id %d no encontrado", typeId)));
        User userToCreate = mapper.convertValue(user, User.class);
        userToCreate.setType(type);
        User userCreated = userRepository.save(userToCreate);
        return mapper.convertValue(userCreated, UserDto.class);
    }

    public UserDto updateUser(Integer id, UserDto user){
        User userToUpdate = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User con id %d no encontrado", id)));
        userToUpdate.setName(user.getName());
        userToUpdate.setLastName(user.getLastName());
        userToUpdate.setAge(user.getAge());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setPassword(user.getPassword());
        return mapper.convertValue(userRepository.save(userToUpdate), UserDto.class);
    }

    public void deleteUser(Integer id){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User con id %d no encontrado", id)));
        userRepository.delete(user);
    }




}

package uia.arqsoft.mexia2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.mexia2.models.Type;

@Repository
public interface TypeRepository extends JpaRepository<Type, Integer> {
}

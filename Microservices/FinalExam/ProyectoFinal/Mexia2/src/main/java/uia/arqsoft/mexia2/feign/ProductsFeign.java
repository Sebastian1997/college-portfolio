package uia.arqsoft.mexia2.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import uia.arqsoft.mexia2.dtos.ProductDto;

import java.util.List;

@FeignClient(value = "ProductsFeign", url = "http://localhost:8081", path = "/products")
public interface ProductsFeign {

    @GetMapping()
    List<ProductDto> getProducts();
}

package uia.arqsoft.mexia2.abstractfactory.product;

public class BienvenidaMenor implements Bienvenida{
    @Override
    public String bienvenidaMañana() {
        return "Buenos dias, recuerda que tus compras deben ser supervisadas por un adulto";
    }

    @Override
    public String bienvenidaTarde() {
        return "Buenas tardes, recuerda que tus compras deben ser supervisadas por un adulto";
    }

    @Override
    public String bienvenidaNoche() {
        return "Buenas noches, recuerda que tus compras deben ser supervisadas por un adulto";
    }
}

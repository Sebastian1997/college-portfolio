package uia.arqsoft.mexia2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.mexia2.dtos.TypeDto;
import uia.arqsoft.mexia2.services.TypeService;

import java.util.List;

@RestController
@RequestMapping("/types")
public class TypeRestController {
    private TypeService typeService;

    @Autowired
    public TypeRestController(TypeService typeService){
        this.typeService = typeService;
    }

    @GetMapping
    public ResponseEntity<List<TypeDto>> getTypes(){
        List<TypeDto> types = typeService.getTypes();
        return new ResponseEntity<>(types, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TypeDto> getTypeById(@PathVariable (name = "id") Integer id){
        TypeDto type = typeService.getTypeById(id);
        return new ResponseEntity<>(type, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TypeDto> create(@RequestBody TypeDto type){
        TypeDto typeCreated = typeService.create(type);
        return new ResponseEntity<>(typeCreated, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TypeDto> updateType(@PathVariable (name = "id") Integer id, @RequestBody TypeDto type){
        TypeDto typeUpdated = typeService.updateType(id, type);
        return new ResponseEntity<>(typeUpdated, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteType(@PathVariable (name = "id") Integer id){
        typeService.deleteType(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



}

package uia.arqsoft.mexia2.dtos;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class TypeDtoProduct {

    private Integer id;

    private String name;
}

package uia.arqsoft.mexia2.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import uia.arqsoft.mexia2.dtos.ProviderDto;

import java.util.List;

@FeignClient(value = "ProvidersFeign", url = "http://localhost:8083", path = "/providers")
public interface ProvidersFeign {

    @GetMapping()
    List<ProviderDto> getProviders();


}





package uia.arqsoft.mexia2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uia.arqsoft.mexia2.dtos.ProductDto;
import uia.arqsoft.mexia2.services.LoginService;

import java.util.List;

@RestController
@RequestMapping("/login")
public class LoginRestController {

    @Autowired
    private LoginService loginService;

    @GetMapping("/{usename}/{password}")
    public String login(@PathVariable (name = "usename") String username, @PathVariable (name = "password") String password){
        return loginService.login(username, password);
    }

    @GetMapping("/productos")
    public List<ProductDto> getProducts(){
        return loginService.getCatalogo();
    }

    @GetMapping("/productos/{name}")
    public List<ProductDto> getProductByName(@PathVariable(name = "name") String name){
        return loginService.getCatalogo(name);
    }
}

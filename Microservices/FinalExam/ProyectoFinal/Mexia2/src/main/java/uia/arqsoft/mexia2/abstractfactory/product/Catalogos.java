package uia.arqsoft.mexia2.abstractfactory.product;

import uia.arqsoft.mexia2.dtos.ProductDto;

import java.util.List;

public interface Catalogos {
    public List<ProductDto> getCatalogo();
}

package uia.arqsoft.mexia2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.mexia2.abstractfactory.factory.MayorService;
import uia.arqsoft.mexia2.abstractfactory.factory.MenorService;
import uia.arqsoft.mexia2.abstractfactory.factory.Servicio;
import uia.arqsoft.mexia2.dtos.ProductDto;
import uia.arqsoft.mexia2.feign.ProductsFeign;
import uia.arqsoft.mexia2.models.User;
import uia.arqsoft.mexia2.repositories.UserRepository;

import java.util.List;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;


    private final ProductsFeign productsFeign;

    public LoginService(ProductsFeign productsFeign) {
        this.productsFeign = productsFeign;
    }


    public String login(String name, String password) {
       User user = userRepository.findByNameAAndPassword(name, password);

        Servicio servicio;

       if (user != null) {
           if(user.getAge() < 18){
                servicio = new MenorService();
                return servicio.getBienvenida();

           }
           else {
               servicio = new MayorService();
                return servicio.getBienvenida() + " " + user.getName();
           }
       } else {
           return "Usuario o contraseña incorrectos";
       }

    }

    public List<ProductDto> getCatalogo(String name){
        User user = userRepository.findByName(name);
        Servicio servicio;
        if(user.getAge() < 18){
            servicio = new MenorService();
            return servicio.getCatalogo(this.productsFeign).getCatalogo();
        }
        else {
            servicio = new MayorService();
            return servicio.getCatalogo(this.productsFeign).getCatalogo();
        }

    }

    public List<ProductDto> getCatalogo(){
        return productsFeign.getProducts();
    }


}

package uia.arqsoft.mexia2.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "type")
//Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column (name = "name")
    private String name;
}

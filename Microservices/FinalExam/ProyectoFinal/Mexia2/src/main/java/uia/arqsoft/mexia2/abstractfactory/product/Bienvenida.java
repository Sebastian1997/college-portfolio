package uia.arqsoft.mexia2.abstractfactory.product;

public interface Bienvenida {
    String bienvenidaMañana();
    String bienvenidaTarde();

    String bienvenidaNoche();

}

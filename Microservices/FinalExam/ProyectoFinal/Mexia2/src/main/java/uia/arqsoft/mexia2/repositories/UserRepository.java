package uia.arqsoft.mexia2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uia.arqsoft.mexia2.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

    @Query("SELECT u FROM User u WHERE u.name = ?1 AND u.password = ?2")
    User findByNameAAndPassword (String username, String password);

    @Query("SELECT u FROM User u WHERE u.name = ?1")
    User findByName (String username);

}

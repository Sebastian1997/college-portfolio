package uia.arqsoft.mexia2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.mexia2.dtos.UserDto;
import uia.arqsoft.mexia2.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService){
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getUsers(){
        List<UserDto> users = userService.getUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer id){
        UserDto user = userService.getUserById(id);
        return ResponseEntity.ok(user);
    }

    //@GetMapping("/login")

    @PostMapping("/{typeid}")
    public ResponseEntity<UserDto> createUser(@PathVariable(name = "typeid") Integer type, @RequestBody UserDto user){
        UserDto userCreated = userService.createUser(type, user);
        return ResponseEntity.ok(userCreated);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable(name = "id") Integer id, @RequestBody UserDto user){
        UserDto userUpdated = userService.updateUser(id, user);
        return ResponseEntity.ok(userUpdated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable(name = "id") Integer id){
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}

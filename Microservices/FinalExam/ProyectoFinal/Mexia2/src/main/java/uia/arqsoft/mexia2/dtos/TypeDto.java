package uia.arqsoft.mexia2.dtos;

import lombok.*;

//Lombok
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class TypeDto {
    private Integer id;
    private String name;
}

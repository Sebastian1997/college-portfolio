package uia.arqsoft.mexia2.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table (name = "user")
//Lombok
@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column (name = "name")
    private String name;

    @Column (name = "last_name")
    private String lastName;

    @Column(name = "age")
    private Integer age;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @OneToOne
    @JoinColumn(name = "user_type", referencedColumnName = "id")
    private Type type;

    public User() {
    }



}

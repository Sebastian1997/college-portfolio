package uia.arqsoft.mexia3;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uia.arqsoft.mexia3.models.Address;
import uia.arqsoft.mexia3.repositories.AddressRepository;


@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
class Mexia3ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private AddressRepository addressRepository;


    @Test
    public void testCreateAddress() throws Exception {
        // crear una dirección de prueba
        Address address = new Address();
        address.setStreet("Capulines");
        address.setNumber(42);
        address.setCity("CDMX");
        address.setState("CDMX");

        // enviar la dirección a la API
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/address")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(address))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();
        Address createdAddress = objectMapper.readValue(responseBody, Address.class);
        assert (createdAddress.getId() != null);

    }

    @Test
    void contextLoads() {
    }

}

package uia.arqsoft.mexia3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import uia.arqsoft.mexia3.models.Address;
import uia.arqsoft.mexia3.models.Provider;

@SpringBootApplication
public class Mexia3Application {

    public static void main(String[] args) {
        SpringApplication.run(Mexia3Application.class, args);
    }

}

package uia.arqsoft.mexia3.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AddressDto {

    private Integer id;

    private String street;

    private Integer number;

    private String city;

    private String state;
}

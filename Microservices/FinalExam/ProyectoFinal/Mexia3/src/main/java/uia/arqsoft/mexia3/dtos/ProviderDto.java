package uia.arqsoft.mexia3.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ProviderDto {

    private Integer id;

    private String name;

    private AddressDto address;

    private String phone;

    private String website;

}

package uia.arqsoft.mexia3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.mexia3.dtos.ProviderDto;
import uia.arqsoft.mexia3.services.ProviderService;

import java.util.List;

@RestController
@RequestMapping("/providers")
public class ProviderRestController {

    private ProviderService providerService;

    @Autowired
    public ProviderRestController(ProviderService providerService) {
        this.providerService = providerService;
    }

    @GetMapping
    public ResponseEntity<List<ProviderDto>> getProviders(){
        return new ResponseEntity<>(providerService.getProviders(), org.springframework.http.HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProviderDto> getProvider(@PathVariable Integer id){
        return new ResponseEntity<>(providerService.getProviderById(id), org.springframework.http.HttpStatus.OK);
    }

    @PostMapping("/local/{id}")
    public ResponseEntity<ProviderDto> createProvider(@PathVariable(name = "id") Integer addressid , @RequestBody ProviderDto providerDto){
        if(addressid == null){
            addressid = 0;
        }
        return new ResponseEntity<>(providerService.createLocalProvider(addressid, providerDto), org.springframework.http.HttpStatus.CREATED);
    }

    @PostMapping("/website")
    public ResponseEntity<ProviderDto> createProvider(@RequestBody ProviderDto providerDto){
        return new ResponseEntity<>(providerService.createWebsiteProvider(providerDto), org.springframework.http.HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProviderDto> updateProvider(@PathVariable Integer id, @RequestBody ProviderDto providerDto){
        return new ResponseEntity<>(providerService.updateProvider(id, providerDto), org.springframework.http.HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteProvider(@PathVariable Integer id){
        providerService.deleteProvider(id);
    }


}

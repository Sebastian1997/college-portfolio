package uia.arqsoft.mexia3.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia3.dtos.ProviderDto;
import uia.arqsoft.mexia3.models.Address;
import uia.arqsoft.mexia3.models.Provider;
import uia.arqsoft.mexia3.repositories.AddressRepository;
import uia.arqsoft.mexia3.repositories.ProviderRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProviderService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ProviderRepository providerRepository;

    ObjectMapper mapper = new ObjectMapper();

    public List<ProviderDto> getProviders(){
        List<Provider> providers = providerRepository.findAll();
        return providers.stream()
                .map(provider -> mapper.convertValue(provider, ProviderDto.class))
                .collect(Collectors.toList());
    }



    public ProviderDto createLocalProvider(Integer addressid, ProviderDto providerDto){

        Provider providerToCreate;

        Address address = addressRepository.findById(addressid)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address con id %d no encontrado", addressid)));

        providerToCreate = new
                Provider.Builder(providerDto.getName())
                .localAndWebsite()
                .setLocalAndWebsite(address, providerDto.getWebsite())
                .setPhone(providerDto.getPhone())
                .build();

        Provider createdProvider = providerRepository.save(providerToCreate);
        return mapper.convertValue(createdProvider, ProviderDto.class);

    }

    public ProviderDto createWebsiteProvider (ProviderDto providerDto){

        Provider providerToCreate;
        providerToCreate = new
                Provider.Builder(providerDto.getName())
                .website()
                .setWebsite(providerDto.getWebsite())
                .setPhone(providerDto.getPhone())
                .build();

        Provider createdProvider = providerRepository.save(providerToCreate);
        return mapper.convertValue(createdProvider, ProviderDto.class);

    }

    public ProviderDto updateProvider(Integer id, ProviderDto providerDto){
        Provider providerToUpdate = providerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Provider con id %d no encontrado", id)));
        providerToUpdate.setName(providerDto.getName());
        providerToUpdate.setWebsite(providerDto.getWebsite());
        providerToUpdate.setPhone(providerDto.getPhone());
        Provider providerUpdated = providerRepository.save(providerToUpdate);
        return mapper.convertValue(providerUpdated, ProviderDto.class);
    }

    public ProviderDto getProviderById(Integer id){
        Provider provider = providerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Provider con id %d no encontrado", id)));
        ProviderDto providerDto = mapper.convertValue(provider, ProviderDto.class);
        return providerDto;
    }

    public void deleteProvider(Integer id){
        Provider providerToDelete = providerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Provider con id %d no encontrado", id)));
        providerRepository.delete(providerToDelete);
    }


}

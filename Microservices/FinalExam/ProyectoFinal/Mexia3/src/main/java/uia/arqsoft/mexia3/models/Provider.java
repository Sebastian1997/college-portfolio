package uia.arqsoft.mexia3.models;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "provider")

//Lombok
@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Provider {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "website")
    private String website;

    public Provider() {

    }


    public static class Builder {
        public class BuilderLocal {
            Builder builder;

            public BuilderLocal(Builder builder) {
                this.builder = builder;
            }

            public Builder setLocalAndWebsite(Address local, String website) {
                provider.setAddress(local);
                provider.setWebsite(website);
                return this.builder;
            }
        }

        public class BuilderWebsite {
            Builder builder;

            public BuilderWebsite(Builder builder) {
                this.builder = builder;
            }

            public Builder setWebsite(String website) {
                provider.setWebsite(website);
                return this.builder;
            }
        }
        public Provider provider;

        public Builder(String name) {
            provider = new Provider();
            provider.name = name;
        }

        public BuilderLocal localAndWebsite() {
            return new BuilderLocal(this);
        }

        public BuilderWebsite website() {
            return new BuilderWebsite(this);
        }

        public Builder setPhone(String phone) {
            provider.setPhone(phone);
            return this;
        }


        public Provider build() {
            return this.provider;
        }

    }

}

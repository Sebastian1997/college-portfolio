package uia.arqsoft.mexia3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.mexia3.models.Provider;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Integer> {
}

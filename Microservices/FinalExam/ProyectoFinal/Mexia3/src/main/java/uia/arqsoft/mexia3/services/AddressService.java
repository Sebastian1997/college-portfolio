package uia.arqsoft.mexia3.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.mexia3.dtos.AddressDto;
import uia.arqsoft.mexia3.models.Address;
import uia.arqsoft.mexia3.repositories.AddressRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressService {
    private AddressRepository addressRepository;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setAddressRepository(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<AddressDto> getAllAddresses() {
        List<Address> addresses = addressRepository.findAll();
        return addresses.stream()
                .map(address -> mapper.convertValue(address, AddressDto.class))
                .collect(Collectors.toList());
    }

    public AddressDto getAddressById(Integer id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address con id %d no encontrado", id)));
        return mapper.convertValue(address, AddressDto.class);
    }

    public AddressDto createAddress(AddressDto addressDto) {
        Address address = mapper.convertValue(addressDto, Address.class);
        Address addressCreated = addressRepository.save(address);
        return mapper.convertValue(addressCreated, AddressDto.class);
    }

    public AddressDto updateAddress(Integer id, AddressDto addressDto) {
        Address addressToUpdate = addressRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address con id %d no encontrado", id)));
        addressToUpdate.setStreet(addressDto.getStreet());
        addressToUpdate.setNumber(addressDto.getNumber());
        addressToUpdate.setCity(addressDto.getCity());
        addressToUpdate.setState(addressDto.getState());
        Address addressUpdated = addressRepository.save(addressToUpdate);
        return mapper.convertValue(addressUpdated, AddressDto.class);
    }

    public void deleteAddress(Integer id) {
        Address addressToDelete = addressRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address con id %d no encontrado", id)));
        addressRepository.delete(addressToDelete);
    }

}

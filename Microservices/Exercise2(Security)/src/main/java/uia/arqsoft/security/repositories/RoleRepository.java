package uia.arqsoft.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.security.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}

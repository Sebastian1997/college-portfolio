package uia.arqsoft.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.security.models.User;
import uia.arqsoft.security.repositories.UserRepository;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getAll(){
        return userRepository.findAll();
    }

    public User getRoleById(Integer id){
        return userRepository.findById(id).get();
    }

    public User create(User user){
        return userRepository.save(user);
    }

    public User update(Integer id, User user) {
        User result = userRepository.findById(id).get();
        result.setUsername(user.getUsername());
        result.setPassword(user.getPassword());
        return userRepository.save(result);
    }

    public void delete(Integer id){
        userRepository.deleteById(id);
    }


}

package uia.arqsoft.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.security.models.Profile;
import uia.arqsoft.security.services.ProfileService;

@RestController
//Esto asegura que no pueda haber un perfil sin un usuario
@RequestMapping("/users/{userid}/profiles")
public class ProfileRestController {

    @Autowired
    private ProfileService profileService;
    @PostMapping
    public ResponseEntity<Profile> createProfile(@PathVariable("userid") Integer userId, @RequestBody Profile profile) {
        return new ResponseEntity<>(profileService.create(userId, profile), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Profile> getProfile(@PathVariable("userid") Integer userId, @PathVariable("id") Integer id) {
        return new ResponseEntity<>(profileService.getById(userId, id), HttpStatus.OK);
    }


}

package uia.arqsoft.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.security.models.Role;
import uia.arqsoft.security.models.User;
import uia.arqsoft.security.models.UserInRole;
import uia.arqsoft.security.repositories.RoleRepository;
import uia.arqsoft.security.repositories.UserInRoleRepository;
import uia.arqsoft.security.repositories.UserRepository;

import java.util.List;

@Service
public class UserInRoleService {
    @Autowired
    private UserInRoleRepository userInRoleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public List<UserInRole> getAll(){
        return userInRoleRepository.findAll();
    }

    public UserInRole getRoleById(Integer id){
        return userInRoleRepository.findById(id).get();
    }

    public UserInRole create(Integer userId, Integer roleId, UserInRole userinRole){
        Role role = roleRepository.findById(roleId).orElseThrow(()-> new ResponseStatusException(
                org.springframework.http.HttpStatus.NOT_FOUND, "Role not found"
        ));

        User user = userRepository.findById(userId).orElseThrow(()-> new ResponseStatusException(
                org.springframework.http.HttpStatus.NOT_FOUND, "User not found"
        ));

        userinRole.setUser(user);
        userinRole.setRole(role);
        return userInRoleRepository.save(userinRole);
    }

    public void delete(Integer id){
        userInRoleRepository.deleteById(id);
    }


}

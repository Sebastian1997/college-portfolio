package uia.arqsoft.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.security.models.UserInRole;
import uia.arqsoft.security.services.UserInRoleService;

@RestController
@RequestMapping("/users/{userid}/roles/{roleid}")
public class UserInRoleControllerRest {
    @Autowired
    private UserInRoleService userInRoleService;

    @GetMapping
    public UserInRole getRoleById(Integer id){
        return userInRoleService.getRoleById(id);
    }

    @PostMapping()
    public UserInRole create(@PathVariable(name = ("userid")) Integer userId, @PathVariable("roleid") Integer roleId, @RequestBody UserInRole userinRole){
        return userInRoleService.create(userId, roleId, userinRole);
    }
}

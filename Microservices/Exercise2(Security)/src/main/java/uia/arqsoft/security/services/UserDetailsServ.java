package uia.arqsoft.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uia.arqsoft.security.models.UserInRole;
import uia.arqsoft.security.repositories.UserInRoleRepository;
import uia.arqsoft.security.repositories.UserRepository;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServ implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserInRoleRepository userInRoleRepository;


    @Autowired
    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<uia.arqsoft.security.models.User> optional = userRepository.findByUsername(username);
        if (optional.isPresent()) {
            uia.arqsoft.security.models.User user = optional.get();
            List<UserInRole> userInRoles =
                    userInRoleRepository.findByUser(user);
            String[] roles =
                    userInRoles.stream()
                            .map(r -> r.getRole().getName())
                            .toArray(String[]::new);
            return
                    org.springframework.security.core.userdetails.
                            User.withUsername(user.getUsername())
                            .password(encoder.encode(user.getPassword()))
                            .roles(roles).build();
        } else {
            throw new
                    UsernameNotFoundException("Username" + username + "not found");

        }
    }


}

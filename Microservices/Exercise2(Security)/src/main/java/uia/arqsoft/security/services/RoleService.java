package uia.arqsoft.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.security.models.Role;
import uia.arqsoft.security.repositories.RoleRepository;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getRoles(){
       return roleRepository.findAll();
    }

    public Role saveRol(Role role){
        return roleRepository.save(role);
    }

    public Role updateRole(Integer id, Role role){
        Role roleToUpdate = roleRepository.findById(id).get();
        roleToUpdate.setName(role.getName());
        return roleRepository.save(roleToUpdate);
    }

    public void deleteRole(Integer id){
        roleRepository.deleteById(id);
    }

}

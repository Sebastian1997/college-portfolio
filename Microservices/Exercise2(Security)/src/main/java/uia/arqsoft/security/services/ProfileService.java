package uia.arqsoft.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uia.arqsoft.security.models.Profile;
import uia.arqsoft.security.models.User;
import uia.arqsoft.security.repositories.ProfileRepository;
import uia.arqsoft.security.repositories.UserRepository;

@Service
public class ProfileService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileRepository profileRepository;

    public Profile create(Integer userid, Profile profile) {
        User result = userRepository.findById(userid).orElse(null);
        if (result != null) {
            profile.setUser(result);
            return profileRepository.save(profile);
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %d not found", userid));
        }
    }

    public Profile getById(Integer userid, Integer profileId) {
        return
                profileRepository.findByUserIdAndProfileId(userid, profileId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Profile %d not found", profileId)));
    }
}

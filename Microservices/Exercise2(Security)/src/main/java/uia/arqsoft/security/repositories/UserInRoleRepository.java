package uia.arqsoft.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.arqsoft.security.models.UserInRole;

import java.util.List;

@Repository
public interface UserInRoleRepository extends JpaRepository<UserInRole, Integer> {
    List<UserInRole> findByUser(uia.arqsoft.security.models.User user);
}

package uia.arqsoft.micros1.repositories;

import uia.arqsoft.micros1.models.TipoRecomendacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoRecomendacionRepository extends JpaRepository<TipoRecomendacion, Integer> {
}

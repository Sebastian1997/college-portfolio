package uia.arqsoft.micros1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.micros1.dtos.RecomendacionDTO;
import uia.arqsoft.micros1.models.Recomendacion;
import uia.arqsoft.micros1.services.RecomendacionService;

import java.util.List;

@RestController
@RequestMapping()
public class RecomendacionRestController {

    @Autowired
    private RecomendacionService recomendacionService;

    @GetMapping("/recomendaciones")
    public ResponseEntity <List<RecomendacionDTO>> getRecomendaciones() {
        return new ResponseEntity<>(recomendacionService.getRecomendaciones(), HttpStatus.OK);
    }

    @PostMapping("/tipo/{id}/recomendaciones")
    public ResponseEntity <RecomendacionDTO> createRecomendacion(@PathVariable (name = "id") Integer id, @RequestBody RecomendacionDTO recomendacion) {
        return new ResponseEntity<>(recomendacionService.createRecomendacion(id, recomendacion), HttpStatus.CREATED);
    }

    @PutMapping("/recomendaciones/{id}")
    public ResponseEntity <RecomendacionDTO> updateRecomendacion(@PathVariable (name = "id") Integer id, @RequestBody RecomendacionDTO recomendacion) {
        return new ResponseEntity<>(recomendacionService.updateRecomendacion(id, recomendacion), HttpStatus.OK);
    }

    @DeleteMapping("/recomendaciones/{id}")
    public ResponseEntity deleteRecomendacion(@PathVariable (name = "id") Integer id) {
        recomendacionService.deleteRecomendacion(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }



}

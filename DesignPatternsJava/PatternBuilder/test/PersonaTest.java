import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.sisoftware.builder.Persona;

public class PersonaTest {
    @Test
    public void test_madre() {
        Persona madre = new org.sisoftware.builder.Persona.Builder("Maria")
                .setMunicipio("Selva")
                .new BuilderMayor()
                .setMayor(30, "Google");
        assertEquals(madre.toString(), "Persona{nombre='Maria', edad=30, municipio='Selva', colegio='null', lugarTrabajo='Google'}");
    }

    @Test
    public void test_hijo() {
        org.sisoftware.builder.Persona hijo = new org.sisoftware.builder.Persona.Builder("Pedro")
                .new BuilderMenor()
                .setMenor(15, "Colegio de Selva");
        assertEquals(hijo.toString(), "Persona{nombre='Pedro', edad=15, municipio='null', colegio='Colegio de Selva', lugarTrabajo='null'}");
    }





}

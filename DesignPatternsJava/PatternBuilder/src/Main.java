public class Main {

	public static void main (String[] args) {

		// este codigo debe compilar
		org.sisoftware.builder.Persona madre = new org.sisoftware.builder.Persona.Builder("Maria")
                .setMunicipio("Selva")
                .new BuilderMayor()
				.setMayor(30, "Google");

		org.sisoftware.builder.Persona hijo = new org.sisoftware.builder.Persona.Builder("Pedro")
                .new BuilderMenor()
				.setMenor(15, "Colegio de Selva");

		System.out.println(madre);

		System.out.println(hijo);


	}
}

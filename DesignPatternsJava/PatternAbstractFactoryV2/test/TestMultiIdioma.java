import org.junit.Test;
import org.sisoftware.abstractfactory.factory.EnService;
import org.sisoftware.abstractfactory.factory.EsService;
import org.sisoftware.abstractfactory.factory.Servicio;
import org.sisoftware.abstractfactory.products.Preguntas;
import org.sisoftware.abstractfactory.products.Saludos;

import static org.junit.Assert.assertEquals;

public class TestMultiIdioma {
	@Test
	public void test_es() {
		Servicio servicioEs = new EsService();
		Preguntas preguntas = servicioEs.getPreguntas();
		Saludos saludos = servicioEs.getSaludos();

		assertEquals("¿qué hora es?", preguntas.preguntaHora() );
		assertEquals("¿qué tiempo hace?", preguntas.preguntaTiempo() );
		assertEquals("buenos días", saludos.buenosDias());
		assertEquals("buenas tardes", saludos.buenasTardes());
	}
	
	@Test
	public void test_en() {
		Servicio servicioEn = new EnService();
		Preguntas preguntas = servicioEn.getPreguntas();
		Saludos saludos = servicioEn.getSaludos();

		assertEquals("what time is it?", preguntas.preguntaHora() );
		assertEquals("how is the weather?", preguntas.preguntaTiempo() );
		assertEquals("good morning", saludos.buenosDias());
		assertEquals("good afternoon", saludos.buenasTardes());
	}
}

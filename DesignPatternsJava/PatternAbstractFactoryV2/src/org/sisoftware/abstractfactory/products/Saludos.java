package org.sisoftware.abstractfactory.products;

public interface Saludos {
	String buenosDias();
	String buenasTardes();
}

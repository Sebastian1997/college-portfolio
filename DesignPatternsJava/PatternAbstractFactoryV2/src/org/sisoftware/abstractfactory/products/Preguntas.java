package org.sisoftware.abstractfactory.products;

public interface Preguntas {
	String preguntaHora();
	String preguntaTiempo();
}

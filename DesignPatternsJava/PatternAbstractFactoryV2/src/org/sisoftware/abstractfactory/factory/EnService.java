package org.sisoftware.abstractfactory.factory;

import org.sisoftware.abstractfactory.products.Preguntas;
import org.sisoftware.abstractfactory.products.PreguntasEn;
import org.sisoftware.abstractfactory.products.Saludos;
import org.sisoftware.abstractfactory.products.SaludosEn;

public class EnService extends Servicio {

    @Override
    public Preguntas getPreguntas() {
        return new PreguntasEn();
    }

    @Override
    public Saludos getSaludos() {
        return new SaludosEn();
    }
}

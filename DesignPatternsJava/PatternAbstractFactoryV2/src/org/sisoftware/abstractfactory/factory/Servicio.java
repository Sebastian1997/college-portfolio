package org.sisoftware.abstractfactory.factory;

import org.sisoftware.abstractfactory.products.Preguntas;
import org.sisoftware.abstractfactory.products.Saludos;

public abstract class Servicio {
    public abstract Preguntas getPreguntas();

    public abstract Saludos getSaludos();
}

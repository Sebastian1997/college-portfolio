package org.sisoftware.abstractfactory.factory;

import org.sisoftware.abstractfactory.products.Preguntas;
import org.sisoftware.abstractfactory.products.PreguntasEs;
import org.sisoftware.abstractfactory.products.Saludos;
import org.sisoftware.abstractfactory.products.SaludosEs;

public class EsService extends Servicio {

    @Override
    public Preguntas getPreguntas() {
        return new PreguntasEs();
    }

    @Override
    public Saludos getSaludos() {
        return new SaludosEs();
    }
}

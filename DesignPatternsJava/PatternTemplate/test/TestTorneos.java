import static org.junit.Assert.*;
import org.junit.Test;
import org.sisoftware.template.*;

public class TestTorneos {
	private Solicitud solicitud_edad_9 = new Solicitud("sol 10", 9, 40);
	private Solicitud solicitud_edad_15_ligero = new Solicitud("sol 15 ligero", 15, 40);
	private Solicitud solicitud_edad_15_pesado = new Solicitud("sol 15 pesado", 15, 70);
	private Polideportivo polideportivo = new Polideportivo();
	private Template lucha = new InscripcionLucha(polideportivo);
	private Template tenis = new InscripcionTenis(polideportivo);

	@Test
	public void test_torneo_tenis_acceptados() {
		assertFalse(tenis.apunta(solicitud_edad_9));
		assertTrue(tenis.apunta(solicitud_edad_15_ligero));
		assertTrue(tenis.apunta(solicitud_edad_15_pesado));
		assertEquals(2, tenis.getTorneo().getAceptadas().size());
	}
	
	@Test
	public void test_torneo_lucha_acceptados() {
		assertFalse(lucha.apunta(solicitud_edad_9));
		assertFalse(lucha.apunta(solicitud_edad_15_ligero));
		assertTrue(lucha.apunta(solicitud_edad_15_pesado));
		assertEquals(1, lucha.getTorneo().getAceptadas().size());
	}

	@Test
	public void test_torneo_tenis_limite() {
		for (int i = 0; i < 4; i++) {
			assertTrue(tenis.apunta(new Solicitud("participante " + 1, 15, 60)));
		}
		assertFalse(tenis.apunta(solicitud_edad_15_ligero));
		Integer horasReservadas = polideportivo.getReservas().get(tenis.getTorneo());
		assertNotNull(horasReservadas);
		assertEquals(8, horasReservadas.intValue());
	}

	@Test
	public void test_torneo_lucha_limite() {
		for (int i = 0; i < 6; i++) {
			assertTrue(lucha.apunta(new Solicitud("participante " + 1, 15, 70)));
		}
		assertFalse(lucha.apunta(solicitud_edad_15_pesado));
		Integer horasReservadas = polideportivo.getReservas().get(lucha.getTorneo());
		assertNotNull(horasReservadas);
		assertEquals(4, horasReservadas.intValue());
	}
}

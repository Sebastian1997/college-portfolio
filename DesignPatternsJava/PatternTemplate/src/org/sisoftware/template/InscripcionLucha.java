package org.sisoftware.template;

public class InscripcionLucha extends Template {
	public InscripcionLucha(Polideportivo polideportivo) {
		super(polideportivo);
		super.setTorneo("torneo lucha");
	}

	@Override
	boolean validaSolicitud(Solicitud solicitud) {
		if (solicitud.getEdat() < 15) {
			// menores de 15 no pueden apuntarse
			return false;
		}
		if (solicitud.getPeso() < 60) {
			// peso minimo, 60 kilos
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	boolean validaPlazas(Solicitud solicitud) {

		if (super.getTorneo().getAceptadas().size() >= 6) {
			// solo pueden participar 6 personas
			return false;
		}
		else {
			return true;
		}
	}


	@Override
	void formalizaTorneo() {
		if (super.getTorneo().getAceptadas().size() == 6) {
			// plazas llenas -> reservamos polideportivo
			// El tiempo estimado del torneo son 4 horas
			super.getPolideportivo().reserva(super.getTorneo(), 4);
		}

	}

}

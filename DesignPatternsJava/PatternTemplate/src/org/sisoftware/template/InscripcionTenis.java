package org.sisoftware.template;

public class InscripcionTenis extends Template{


	public InscripcionTenis(Polideportivo polideportivo) {
		super(polideportivo);
		super.setTorneo("torneo tenis");
	}


	//public Torneo getTorneo() {return torneo;}

	@Override
	boolean validaSolicitud(Solicitud solicitud) {
		if (solicitud.getEdat() < 10) {
			// menores de 10 no pueden apuntarse
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	boolean validaPlazas(Solicitud solicitud) {

		if (super.getTorneo().getAceptadas().size() >= 4) {
			// solo pueden participar 4 personas
			return false;
		}else{

		return true;
		}
	}

	@Override
	void formalizaTorneo() {

		if (super.getTorneo().getAceptadas().size() == 4) {
			// plazas llenas -> reservamos polideportivo
			// El tiempo estimado del torneo son 8 horas
			super.getPolideportivo().reserva(super.getTorneo(), 8);
		}

	}

	
}

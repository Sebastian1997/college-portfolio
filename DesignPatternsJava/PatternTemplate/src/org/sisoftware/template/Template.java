package org.sisoftware.template;

public abstract class Template {

    private Torneo torneo;
    private final Polideportivo polideportivo;

    public Template(Polideportivo polideportivo) {
        this.polideportivo = polideportivo;
    }

    protected Template(Torneo torneo, Polideportivo polideportivo) {
        this.torneo = torneo;
        this.polideportivo = polideportivo;
    }


    public final boolean apunta(Solicitud solicitud){

        if(!validaSolicitud(solicitud)){
            return false;
        }

        if(!validaPlazas(solicitud)){
            return false;
        }

        apuntaSolicitud(solicitud);


        formalizaTorneo();


        return true;
    }

    abstract boolean validaSolicitud(Solicitud solicitud);
    abstract boolean validaPlazas(Solicitud solicitud);
    void apuntaSolicitud(Solicitud solicitud){
        torneo.apunta(solicitud);
    }

    abstract void formalizaTorneo();


    public void setTorneo(String nombre) {
        this.torneo = new Torneo(nombre);
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public Polideportivo getPolideportivo() {
        return polideportivo;
    }
}

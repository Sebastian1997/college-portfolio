import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import org.sisoftware.command.PedidoInternacional;
import org.sisoftware.command.TratamientoPedido;
import org.sisoftware.command.TratamientoPedidoInternacional;

public class TestPedidoInternacional {

	@Test
	public void test() {
		// Crear una clase TratamientoPedidoInternacional que permita tratar pedidos internacionales
		// La clase debe permitir tratar todos los pedidos excepto los que van a Mordor
		
		// Descomentar el codigo y ejecutar el test

		TratamientoPedido tratamientoKO = new TratamientoPedidoInternacional(new PedidoInternacional("Mordor", 100));
		assertFalse(tratamientoKO.tratar());
		
		TratamientoPedido tratamientoOK = new TratamientoPedidoInternacional(new PedidoInternacional("Comarca", 100));
		assertTrue(tratamientoOK.tratar());

	}
}

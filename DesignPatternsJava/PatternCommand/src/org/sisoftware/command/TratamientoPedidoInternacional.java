package org.sisoftware.command;

public class TratamientoPedidoInternacional implements TratamientoPedido{

    private PedidoInternacional pedido;


    public TratamientoPedidoInternacional(PedidoInternacional pedido) {
        this.pedido = pedido;
    }

    @Override
    public boolean tratar() {
        if(pedido.getDestino().equals("Mordor")){
            return false;
        }
        else {
            return true;
        }
    }
}

package org.sisoftware.factorymethod.creators;

import org.sisoftware.factorymethod.products.Lavadora;

public abstract class LavadoraFactory {
    public Lavadora getLavadora(){
        Lavadora lavadora = crearLavadora();
        lavadora.ponerMandos();
        lavadora.ponerTambor();
        return lavadora;

    }

    public abstract Lavadora crearLavadora();
}

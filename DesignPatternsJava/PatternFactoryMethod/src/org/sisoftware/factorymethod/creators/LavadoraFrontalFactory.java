package org.sisoftware.factorymethod.creators;

import org.sisoftware.factorymethod.products.Lavadora;
import org.sisoftware.factorymethod.products.LavadoraCargaFrontal;

public class LavadoraFrontalFactory extends LavadoraFactory {

    @Override
    public Lavadora crearLavadora() {
        return new LavadoraCargaFrontal();
    }

}

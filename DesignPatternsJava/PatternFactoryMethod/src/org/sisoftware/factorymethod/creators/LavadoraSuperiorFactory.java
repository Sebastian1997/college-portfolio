package org.sisoftware.factorymethod.creators;

import org.sisoftware.factorymethod.products.Lavadora;
import org.sisoftware.factorymethod.products.LavadoraCargaSuperior;

public class LavadoraSuperiorFactory extends LavadoraFactory {

    @Override
    public Lavadora crearLavadora() {

        return new LavadoraCargaSuperior();
    }

}

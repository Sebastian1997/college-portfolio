import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.sisoftware.factorymethod.creators.LavadoraFactory;
import org.sisoftware.factorymethod.creators.LavadoraFrontalFactory;
import org.sisoftware.factorymethod.creators.LavadoraSuperiorFactory;
import org.sisoftware.factorymethod.products.Lavadora;

public class TestLavadora {
	@Test
	public void test_carga_frontal() {
		LavadoraFactory lavadoraF = new LavadoraFrontalFactory();
		Lavadora lavadora = lavadoraF.getLavadora();
		assertEquals("frontal", lavadora.tipoCarga);
		assertTrue(lavadora.tieneMandos);
		assertTrue(lavadora.tieneTambor);
	}
	@Test
	public void test_carga_superior() {
		LavadoraFactory lavadoraS = new LavadoraSuperiorFactory();
		Lavadora lavadora = lavadoraS.getLavadora();
		assertEquals("superior", lavadora.tipoCarga);
		assertTrue(lavadora.tieneMandos);
		assertTrue(lavadora.tieneTambor);
	}
}

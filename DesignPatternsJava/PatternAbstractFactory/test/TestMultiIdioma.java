import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.sisoftware.abstractfactory.*;

public class TestMultiIdioma {
	@Test
	public void test_es() {
		FactoryProducer fp = new FactoryProducer("es");
		assertEquals("¿qué hora es?", fp.getFactory("preguntas").getPreguntas().preguntaHora());
		assertEquals("¿qué tiempo hace?", fp.getFactory("preguntas").getPreguntas().preguntaTiempo());
		assertEquals("buenos días", fp.getFactory("saludos").getSaludos().buenosDias());
		assertEquals("buenas tardes", fp.getFactory("saludos").getSaludos().buenasTardes());
	}
	
	@Test
	public void test_en() {
		FactoryProducer fp1 = new FactoryProducer("en");
		assertEquals("what time is it?", fp1.getFactory("preguntas").getPreguntas().preguntaHora());
		assertEquals("how is the weather?", fp1.getFactory("preguntas").getPreguntas().preguntaTiempo() );
		assertEquals("good morning", fp1.getFactory("saludos").getSaludos().buenosDias());
		assertEquals("good afternoon", fp1.getFactory("saludos").getSaludos().buenasTardes());
	}
}

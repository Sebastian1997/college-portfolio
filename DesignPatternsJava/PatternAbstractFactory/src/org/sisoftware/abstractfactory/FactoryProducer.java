package org.sisoftware.abstractfactory;

public class FactoryProducer {


    private String idioma;

    public FactoryProducer(String idioma) {
        this.idioma = idioma;
    }

    public AbstractFactory getFactory(String tipo) {
        if (tipo.equals("preguntas")) {
            return new PreguntasFactory(idioma);
        } else if (tipo.equals("saludos")) {
            return new SaludosFactory(idioma);
        } else {
            return null;
        }
    }
}

package org.sisoftware.abstractfactory;

public interface Saludos {
	String buenosDias();
	String buenasTardes();
}

package org.sisoftware.abstractfactory;

public interface AbstractFactory {
    Preguntas getPreguntas();
    Saludos getSaludos();

}

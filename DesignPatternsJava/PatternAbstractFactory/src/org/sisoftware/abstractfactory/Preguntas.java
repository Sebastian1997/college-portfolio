package org.sisoftware.abstractfactory;

public interface Preguntas {
	String preguntaHora();
	String preguntaTiempo();
}

package org.sisoftware.abstractfactory;

public class SaludosFactory implements AbstractFactory {

    private String idioma;

    public SaludosFactory(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public Preguntas getPreguntas() {
        return null;
    }

    @Override
    public Saludos getSaludos() {
        if (idioma.equals("es")) {
            return new SaludosEs();
        } else if (idioma.equals("en")) {
            return new SaludosEn();
        } else {
            return null;
        }
    }


}

package org.sisoftware.abstractfactory;

public class PreguntasFactory implements AbstractFactory {

    private String idioma;

    public PreguntasFactory(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public Preguntas getPreguntas() {
        if (idioma.equals("es")) {
            return new PreguntasEs();
        } else if (idioma.equals("en")) {
            return new PreguntasEn();
        } else {
            return null;
        }

    }

    @Override
    public Saludos getSaludos() {
        return null;
    }


}

package org.sisoftware.adapter;

public class Adapter implements InternationalMoneyOrganization{

    private final BancoNostrum bancoNostrum;
    public Adapter(BancoNostrum bancoNostrum) {
        this.bancoNostrum = bancoNostrum;
    }

    @Override
    public void transfer(int cantidad, String cliente) {
        transferToMovimiento(cantidad, cliente);
    }

    @Override
    public int state(String cliente) {
        return stateToEstado(cliente);
    }

    private void transferToMovimiento(int cantidad, String cliente) {
        bancoNostrum.movimiento(cliente, cantidad);
    }

    private int stateToEstado(String cliente) {
        Integer estado = bancoNostrum.estado(cliente);
        if (estado == null) {
            estado = 0;
        }
        return estado;
    }



}

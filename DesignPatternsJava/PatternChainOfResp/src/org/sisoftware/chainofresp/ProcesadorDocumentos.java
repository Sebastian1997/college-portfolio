package org.sisoftware.chainofresp;

import java.util.Arrays;
import java.util.List;

public class ProcesadorDocumentos {

    private List<LectorDocumentos> lista;

    public ProcesadorDocumentos(LectorDocumentos ... lectorDocumentos ) {
        lista = Arrays.asList(lectorDocumentos);
    }

    public String concatena(List<Documento> documentos) {
        String resultado = "";
        for (Documento doc : documentos) {
            for (LectorDocumentos docu : lista){
                if(docu.valida(doc.getTipo())) {
                    resultado += docu.contenido(doc);
                }
            }
            resultado += "\n";
        }
        return resultado;
    }

}

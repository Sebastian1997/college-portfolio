package org.sisoftware.chainofresp;

public class LectorOdt implements LectorDocumentos {
	@Override
	public String contenido(Documento documento) {
		return "odt " + documento.getContenido();
	}

	@Override
	public Boolean valida(String type) {
		return type.equals("odt");
	}


}

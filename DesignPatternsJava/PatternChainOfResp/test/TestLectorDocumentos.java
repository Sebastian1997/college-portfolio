import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.sisoftware.chainofresp.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class TestLectorDocumentos {
    @Test
    public void test() {

        ProcesadorDocumentos procesador = new ProcesadorDocumentos(new LectorDoc(), new LectorPdf(), new LectorOdt());

        List<Documento> lista = Arrays.asList(
                new Documento("doc", "documento doc"),
                new Documento("pdf", "documento pdf"),
                new Documento("odt", "documento odt")
        );


        String resultado =  procesador.concatena(lista);
        String esperado = "doc documento doc\n" +
                "pdf documento pdf\n" +
                "odt documento odt\n";
        assertEquals(esperado, resultado);
    }
}

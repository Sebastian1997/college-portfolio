package org.sisoftware.builder;

public class Persona {
    private String nombre;
    private int edad;
    private String municipio;
    private String colegio;
    private String lugarTrabajo;

    private Persona() {
    }

    public static class Builder {
        public class BuilderMenor {
            Builder builder;
            public BuilderMenor(Builder builder) {
                this.builder = builder;
            }
            public Builder setColegio(String colegio) {
                persona.colegio = colegio;
                return this.builder;
            }

        }

        public class BuilderMayor {
            Builder builder;
            public BuilderMayor(Builder builder) {
                this.builder = builder;
            }
            public Builder setLugarTrabajo(String lugarTrabajo) {
                persona.lugarTrabajo = lugarTrabajo;
                return this.builder;
            }

        }

        public Persona persona;
        public Builder(String nombre) {
            persona = new Persona();
            persona.nombre = nombre;
        }

        public Builder setMunicipio(String municipio) {
            persona.municipio = municipio;
            return this;
        }

        public BuilderMayor setMayor(int edad) {
            if(edad < 18) {
                throw new IllegalArgumentException("La edad debe ser mayor a 18");
            }
            persona.edad = edad;
            return new BuilderMayor(this);
        }

        public BuilderMenor setMenor(int edad) {
            if(edad >= 18) {
                throw new IllegalArgumentException("La edad debe ser menor a 18");
            }
            persona.edad = edad;
            return new BuilderMenor(this);
        }


        public Persona build() {
            return persona;
        }

    }

    @Override
    public String toString() {
        return "Persona [nombre=" + nombre + ", edad=" + edad + ", municipio=" + municipio + ", colegio=" + colegio
                + ", lugarTrabajo=" + lugarTrabajo + "]";
    }

}

import org.junit.Test;
import org.sisoftware.builder.Persona;
import static org.junit.Assert.assertEquals;


class PersonaTest {
    @Test
    public void test_madre() {
        Persona madre = new org.sisoftware.builder.Persona.Builder("Maria")
                .setMunicipio("Selva")
                .setMayor(30).setLugarTrabajo("Google").build();
        assertEquals(madre.toString(), "Persona{nombre='Maria', edad=30, municipio='Selva', colegio='null', lugarTrabajo='Google'}");
    }

    @Test
    public void test_hijo() {
        org.sisoftware.builder.Persona hijo = new org.sisoftware.builder.Persona.Builder("Pedro")
                .setMenor(15).setColegio("Colegio de Selva").build();
        assertEquals(hijo.toString(), "Persona{nombre='Pedro', edad=15, municipio='null', colegio='Colegio de Selva', lugarTrabajo='null'}");
    }


}
package org.sisoftware.mediator;


public class Telefono implements Component {

	private Mediator mediator;
	private boolean on = false;
	@Override
	public void setMediator(Mediator mediator) {
		this.mediator = mediator;
	}

	@Override
	public void enciende() {
		on = true;
	}

	public void enciendeMusica(){
		mediator.enciendeTelefono();
	}

	public void recibeLlamada() {
		mediator.apagaRadio();
		mediator.enciendeTelefono();

	}

	@Override
	public void apaga() {
		on = false;
	}

	@Override
	public boolean status() {
		return on;
	}

	public boolean musicaEncendida(){
		return on;
	}

}

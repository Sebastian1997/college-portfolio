package org.sisoftware.mediator;

public class Radio implements Component{

	private Mediator mediator;
	private boolean on = false;
	@Override
	public void setMediator(Mediator mediator) {
		this.mediator = mediator;
	}

	@Override
	public void enciende() {
		on = true;
	}

	public void enciendeR() {
		mediator.enciendeRadio();
		mediator.apagaTelefono();
	}

	@Override
	public void apaga() {
		on = false;
	}

	@Override
	public boolean status() {
		return on;
	}

	public boolean encendida() {
		return on;
	}

}

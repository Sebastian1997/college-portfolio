package org.sisoftware.mediator;

import javax.print.attribute.standard.Media;
import java.awt.*;

public class Coche implements Component{

    private Mediator mediator;
    private boolean on = false;
    @Override
    public void setMediator(Mediator mediator) {
        this.mediator=mediator;
    }

    @Override
    public void enciende() {
        on = true;
    }

    public void cocheEnciende() {
        mediator.enciendeCoche();
        mediator.enciendeRadio();
        mediator.apagaTelefono();
    }

    @Override
    public void apaga() {
        on = false;
    }

    public void apagaC() {
        mediator.apagaRadio();
        mediator.apagaCoche();
    }


    @Override
    public boolean status() {
        return on;
    }




	
}
